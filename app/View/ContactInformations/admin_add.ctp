<div class="contactInformations form">
<?php echo $this->Form->create('ContactInformation'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Contact Information'); ?></legend>
	<?php
		echo $this->Form->input('first_address');
		echo $this->Form->input('second_address');
		echo $this->Form->input('town');
		echo $this->Form->input('country');
		echo $this->Form->input('phone_1');
		echo $this->Form->input('phone_2');
		echo $this->Form->input('mail_1');
		echo $this->Form->input('mail_2');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contact Informations'), array('action' => 'index')); ?></li>
	</ul>
</div>
