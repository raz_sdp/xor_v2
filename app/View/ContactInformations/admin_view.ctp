<div class="contactInformations view">
<h2><?php echo __('Contact Information'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Address'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['first_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Second Address'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['second_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Town'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['town']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone 1'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['phone_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone 2'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['phone_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mail 1'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['mail_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mail 2'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['mail_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($contactInformation['ContactInformation']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contact Information'), array('action' => 'edit', $contactInformation['ContactInformation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contact Information'), array('action' => 'delete', $contactInformation['ContactInformation']['id']), array(), __('Are you sure you want to delete # %s?', $contactInformation['ContactInformation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Informations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Information'), array('action' => 'add')); ?> </li>
	</ul>
</div>
