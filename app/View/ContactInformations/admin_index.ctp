<div class="contactInformations index">
	<h2><?php echo __('Contact Informations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('first_address'); ?></th>
			<th><?php echo $this->Paginator->sort('second_address'); ?></th>
			<th><?php echo $this->Paginator->sort('town'); ?></th>
			<th><?php echo $this->Paginator->sort('country'); ?></th>
			<th><?php echo $this->Paginator->sort('phone_1'); ?></th>
			<th><?php echo $this->Paginator->sort('phone_2'); ?></th>
			<th><?php echo $this->Paginator->sort('mail_1'); ?></th>
			<th><?php echo $this->Paginator->sort('mail_2'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($contactInformations as $contactInformation): ?>
	<tr>
		<td><?php echo h($contactInformation['ContactInformation']['id']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['first_address']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['second_address']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['town']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['country']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['phone_1']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['phone_2']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['mail_1']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['mail_2']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['created']); ?>&nbsp;</td>
		<td><?php echo h($contactInformation['ContactInformation']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contactInformation['ContactInformation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contactInformation['ContactInformation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contactInformation['ContactInformation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $contactInformation['ContactInformation']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contact Information'), array('action' => 'add')); ?></li>
	</ul>
</div>
