<div class="subMenus view">
<h2><?php echo __('Sub Menu'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Menu'); ?></dt>
		<dd>
			<?php echo $this->Html->link($subMenu['Menu']['name'], array('controller' => 'menus', 'action' => 'view', $subMenu['Menu']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Link'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($subMenu['SubMenu']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sub Menu'), array('action' => 'edit', $subMenu['SubMenu']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sub Menu'), array('action' => 'delete', $subMenu['SubMenu']['id']), array(), __('Are you sure you want to delete # %s?', $subMenu['SubMenu']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sub Menus'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sub Menu'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Menus'), array('controller' => 'menus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Menu'), array('controller' => 'menus', 'action' => 'add')); ?> </li>
	</ul>
</div>
