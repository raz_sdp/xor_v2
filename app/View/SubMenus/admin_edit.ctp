<div class="subMenus form">
<?php echo $this->Form->create('SubMenu'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Sub Menu'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('menu_id');
		echo $this->Form->input('name');
		echo $this->Form->input('link');
		echo $this->Form->input('order');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SubMenu.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SubMenu.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sub Menus'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Menus'), array('controller' => 'menus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Menu'), array('controller' => 'menus', 'action' => 'add')); ?> </li>
	</ul>
</div>
