<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="subMenus form">
<?php echo $this->Form->create('SubMenu'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Sub Menu of menu: '); echo $menu_id; ?></legend>
	<?php
		echo $this->Form->input('menu_id', array('type'=>'hidden', 'value'=>$menu_id));
		echo $this->Form->input('name');
		echo $this->Form->input('link');
		echo $this->Form->input('order');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sub Menus'), array('action' => 'sub_menus', $menu_id)); ?></li>
		<li><?php echo $this->Html->link(__('List Menus'), array('controller' => 'menus', 'action' => 'index')); ?> </li>
	</ul>
</div>
