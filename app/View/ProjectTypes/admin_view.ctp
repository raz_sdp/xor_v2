<div class="projectTypes view">
<h2><?php echo __('Project Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectType['ProjectType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($projectType['ProjectType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectType['ProjectType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectType['ProjectType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Type'), array('action' => 'edit', $projectType['ProjectType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Type'), array('action' => 'delete', $projectType['ProjectType']['id']), array(), __('Are you sure you want to delete # %s?', $projectType['ProjectType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
