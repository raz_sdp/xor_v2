<div class="teamMembers view">
<h2><?php echo __('Team Member'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Full Name'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['full_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Display Name'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['display_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image Link'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['image_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Designation'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['designation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Work Role'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['work_role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact 1'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['contact_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact 2'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['contact_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mail 1'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['mail_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mail 2'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['mail_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($teamMember['TeamMember']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Team Member'), array('action' => 'edit', $teamMember['TeamMember']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Team Member'), array('action' => 'delete', $teamMember['TeamMember']['id']), array(), __('Are you sure you want to delete # %s?', $teamMember['TeamMember']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Team Members'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Team Member'), array('action' => 'add')); ?> </li>
	</ul>
</div>
