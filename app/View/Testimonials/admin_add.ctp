<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="testimonials form">
<?php echo $this->Form->create('Testimonial', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Testimonial'); ?></legend>
	<?php
		echo $this->Form->input('comment');
		echo $this->Form->input('name');
		echo $this->Form->input('organization');
		echo $this->Form->input('image_link', array('type' => 'file'));
        echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Testimonials'), array('action' => 'index')); ?></li>
	</ul>
</div>
