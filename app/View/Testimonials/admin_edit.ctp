<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="testimonials form">
<?php echo $this->Form->create('Testimonial', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Testimonial'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('comment');
		echo $this->Form->input('name');
		echo $this->Form->input('organization');
        if(!empty($this->request->data['Testimonial']['image_link'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/client/' . $this->request->data['Testimonial']['image_link'], array('alt'=>'picture', 'width'=> '150px','height' => '150px'));
            echo "</div>";
        }
        echo $this->Form->input('image_link', array('type' => 'file'));
        echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Testimonial.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Testimonial.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Testimonials'), array('action' => 'index')); ?></li>
	</ul>
</div>
