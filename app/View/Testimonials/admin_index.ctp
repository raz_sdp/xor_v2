<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="testimonials index">
	<h2><?php echo __('Testimonials'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('comment'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('organization'); ?></th>
			<th><?php echo $this->Paginator->sort('image_link'); ?></th>
            <th><?php echo $this->Paginator->sort('active'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($testimonials as $testimonial): ?>
	<tr>
		<td><?php echo h($testimonial['Testimonial']['comment']); ?>&nbsp;</td>
		<td><?php echo h($testimonial['Testimonial']['name']); ?>&nbsp;</td>
		<td><?php echo h($testimonial['Testimonial']['organization']); ?>&nbsp;</td>
		<td><?php echo h($testimonial['Testimonial']['image_link']); ?>&nbsp;</td>
        <td><?php echo h($testimonial['Testimonial']['active']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $testimonial['Testimonial']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $testimonial['Testimonial']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $testimonial['Testimonial']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Testimonial'), array('action' => 'add')); ?></li>
	</ul>
</div>
