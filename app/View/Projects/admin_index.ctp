<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="projects index">
	<h2><?php echo __('Projects'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('short_desc'); ?></th>
			<th><?php echo $this->Paginator->sort('project_type'); ?></th>
            <th><?php echo $this->Paginator->sort('client'); ?></th>
			<th><?php echo $this->Paginator->sort('live_demo_link'); ?></th>
			<th><?php echo $this->Paginator->sort('google_link'); ?></th>
			<th><?php echo $this->Paginator->sort('apple_link'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projects as $project): ?>
	<tr>
		<td><?php echo h($project['Project']['name']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['status']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['short_desc']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['project_type']); ?>&nbsp;</td>
        <td><?php echo h($project['Project']['client_name']); ?>&nbsp;<?php echo ", ";echo h($project['Project']['client_organization']); ?></td>
		<td><?php echo $this->Html->image("demo.png", array(
                    "alt" => $project['Project']['live_demo_link'],
                    "height" => "30px",
                    "width" => "30px",
                    'url' => $project['Project']['live_demo_link']
                )
            );?>&nbsp;</td>
		<td><?php echo $this->Html->image("droid.png", array(
                    "alt" => $project['Project']['google_link'],
                    "height" => "30px",
                    "width" => "30px",
                    'url' => $project['Project']['google_link']
                )
            );?>&nbsp;</td>
		<td><?php echo $this->Html->image("apple.png", array(
                    "alt" => $project['Project']['apple_link'],
                    "height" => "30px",
                    "width" => "30px",
                    'url' => $project['Project']['apple_link']
                )
            );?>&nbsp;</td>
		<td class="actions">
            <?php echo $this->Html->link(__('Add to Portfolio'), array('action' => 'add_to_portfolio', $project['Project']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?></li>
	</ul>
</div>
