<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="partners form">
<?php echo $this->Form->create('Partner', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Partner'); ?></legend>
	<?php
		echo $this->Form->input('name');
        echo $this->Form->input('link');
		echo $this->Form->input('logo', array('type' => 'file'));
        echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Partners'), array('action' => 'index')); ?></li>
	</ul>
</div>
