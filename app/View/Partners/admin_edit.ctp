<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="partners form">
<?php echo $this->Form->create('Partner', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Partner'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
        echo $this->Form->input('link');
        if(!empty($this->request->data['Partner']['logo'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/partners/' . $this->request->data['Partner']['logo'], array('alt'=>'picture', 'width'=> '150px','height' => '150px'));
            echo "</div>";
        }
		echo $this->Form->input('logo', array('type' => 'file'));
        echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Partner.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Partner.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Partners'), array('action' => 'index')); ?></li>
	</ul>
</div>
