<!-- footer start -->
<footer id="contact" class="footer-widget-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-md-offset-1">
                <div class="footer-widget footer-logo">

                </div><!-- /.col-md-6 -->
            </div><!-- /.col-md-6 -->
            <div class="col-sm-3 col-md-2 col-lg-2">
                <div class="footer-widget">
                    <h3>Social</h3>
                    <ul>
                        <?php foreach($social_links as $item) { ?>
                        <li><a href="<?php echo $item['SocialLink']['link'];?>"><?php echo $item['SocialLink']['name'];?></a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
            <div class="col-sm-3 col-md-3">
                <div class="footer-widget">
                    <h3>Location</h3>
                    <address>
                        <?php echo $contact_information['ContactInformation']['first_address'].', </br>'.$contact_information['ContactInformation']['second_address'] ?><br>
                        <?php echo $contact_information['ContactInformation']['town'].', '.$contact_information['ContactInformation']['country'] ?><br>
                        <!-- Google Map Modal Trigger -->
                        <a class="modal-map" data-toggle="modal" data-target="#cssMapModal" href="#">View on Google Maps</a>
                    </address>


                    <!-- Modal -->
                    <div class="modal fade" id="cssMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Our Location</h4>
                                </div>
                                <div class="modal-body">

                                    <div id="googleMap"></div>

                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- End Modal -->

                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
            <div class="col-sm-3 col-md-3">
                <div class="footer-widget">
                    <h3>Get in Touch</h3>
                    <a href="#"><?php echo $contact_information['ContactInformation']['phone_1'];?></a>
                    <a href="mailto:#"><?php echo $contact_information['ContactInformation']['mail_1'];?></a>
                    <a class="feedback-modal" data-toggle="modal" data-target="#feedModal" href="#">Send us your feedback</a>

                    <!-- Modal -->
                    <div class="modal fade" id="feedModal" tabindex="-1" role="dialog" aria-labelledby="feedModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="feedModalLabel">Send us your feedback</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="contactForm" action="<?php echo $this->Html->url('/')?>" method="POST">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input id="name" name="name" type="text" class="form-control"  required="" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email address</label>
                                                    <input id="email" name="email" type="email" class="form-control" required="" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone">Phone</label>
                                                    <input id="phone" name="phone" type="text" class="form-control" placeholder="Phone">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="subject">Subject</label>
                                                    <input id="subject" name="subject" type="text" class="form-control" required="" placeholder="Subject">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group text-area">
                                            <label for="message">Message</label>
                                            <textarea id="message" name="message" class="form-control" rows="6" required="" placeholder="Message"></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="copyright">
                    <p>&copy; <?php echo date('Y')?> XOR.  All rights reserved.</p>
                </div>
            </div>
        </div>

    </div><!-- /.container -->
</footer>
<!-- footer end -->