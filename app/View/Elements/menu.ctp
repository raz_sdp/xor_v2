<?php
    $controller = $this->params['controller'];
    $action = $this->params['action'];
    //$method = $this->params['pass'][0];

    $settings_group = array("generalSettings", "contactInformations", "socialLinks", "sliderLayers", "sliderImages");
?>
<div class="actions">
	<h3><?php echo __('Dashboard'); ?></h3>
	<ul>
	    <li class="menu <?php if(in_array($controller,$settings_group)) echo "selected";?>" >
            <?php echo $this->Html->link(__('Settings'), array('controller' => 'generalSettings', 'action' => 'edit', 1, 'admin'=>true)); ?>
            <?php if(in_array($controller,$settings_group)) { ?>
            <ul style="margin-top: 10px;">
                <li class="sub-menu <?php if( $controller == 'generalSettings') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('General Settings'), array('controller' => 'generalSettings', 'action' => 'edit', 1, 'admin'=>true)); ?>
                </li>
                <li class="sub-menu <?php if($controller == 'contactInformations') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('Contact Info Settings'), array('controller' => 'contactInformations', 'action' => 'edit', 1, 'admin'=>true)); ?>
                </li>
                <li class="sub-menu <?php if($controller == 'socialLinks') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('Social Link Settings'), array('controller' => 'socialLinks', 'action' => 'index', 'admin'=>true)); ?>
                </li>
                <li class="sub-menu <?php if($controller == 'sliderImages') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('Slider Settings'), array('controller' => 'sliderImages', 'action' => 'general_settings', 'admin'=>true)); ?>
                    <?php if($controller == 'sliderImages' || $controller == 'sliderLayers'){ ?>
                    <ul style="margin-top: 10px;">
                        <li class="sub-menu <?php if( $controller == 'sliderImages' && $action == 'admin_general_settings') echo "selected3";?> ">
                            <?php echo $this->Html->link(__('General'), array('controller' => 'sliderImages', 'action' => 'general_settings', 'admin'=>true)); ?>
                        </li>
                        <li class="sub-menu <?php if($controller == 'sliderImages' && $action != 'admin_general_settings') echo "selected3";?> ">
                            <?php echo $this->Html->link(__('Images'), array('controller' => 'sliderImages', 'action' => 'index', 'admin'=>true)); ?>
                        </li>
                        <li class="sub-menu <?php if($controller == 'sliderImages' && $action=='preview') echo "selected3";?> ">
                            <?php echo $this->Html->link(__('Preview'), array('controller' => 'sliderImages', 'action' => 'preview', 'admin'=>true)); ?>
                        </li>
                    </ul>
                    <?php } ?>
                </li>
            </ul>
            <?php } ?>
        </li>
		<li class="menu <?php if( $controller == 'menus' ) echo "selected";?> ">
            <?php echo $this->Html->link(__('Menus'), array('controller' => 'menus', 'action' => 'index')); ?>
        </li>
        <li class="menu <?php if( $controller == 'services') echo "selected";?> ">
            <?php echo $this->Html->link(__('Services'), array('controller' => 'services', 'action' => 'index')); ?>
        </li>
		<li class="menu <?php if( $controller == 'projects' || $controller == 'portfolios') echo "selected";?> ">
            <?php echo $this->Html->link(__('Projects'), array('controller' => 'projects', 'action' => 'index')); ?>
            <?php if($controller == 'projects' || $controller == 'portfolios'){ ?>
                <ul style="margin-top: 10px;">
                    <li class="sub-menu <?php if( $controller == 'portfolios') echo "selected2";?> ">
                        <?php echo $this->Html->link(__('Portfolios'), array('controller' => 'portfolios', 'action' => 'index')); ?>
                    </li>
                </ul>
            <?php } ?>
        </li>
		<li class="menu <?php if( $controller == 'teamMembers') echo "selected";?> ">
            <?php echo $this->Html->link(__('Team'), array('controller' => 'teamMembers', 'action' => 'index')); ?>
        </li>
        <li class="menu <?php if( $controller == 'testimonials') echo "selected";?> ">
            <?php echo $this->Html->link(__('Testimonials'), array('controller' => 'testimonials', 'action' => 'index')); ?>
        </li>
        <li class="menu <?php if( $controller == 'partners') echo "selected";?> ">
            <?php echo $this->Html->link(__('Partners'), array('controller' => 'partners', 'action' => 'index')); ?>
        </li>
		<li class="menu <?php if( $controller == 'users') echo "selected";?> ">
            <?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?>
        </li>

		<!--<li><?php /*echo $this->Html->link(__('Hotspots'), array('controller' => 'hotspots', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('Shares'), array('controller' => 'shares', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('Media'), array('controller' => 'media', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('User'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('Tours User'), array('controller' => 'tours_users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('Settings'), array('controller' => 'settings', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('Push Notification'), array('controller' => 'users', 'action' => 'push')); */?> </li>
        <li><?php /*echo $this->Html->link(__('Promoters'), array('controller' => 'promoters', 'action' => 'index')); */?> </li>-->

        <li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
	</ul>
</div>