<div class="col-md-12">
<div class="" style="margin-top:15px;">
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active col-md-4 text-center"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Web Plan</a></li>
    <li role="presentation" class="text-center col-md-4"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Android & Java Plan</a></li>
    <li role="presentation" class="col-md-4 text-center"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">iOS Plan</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="profile">
<div class="col-md-12" style="margin-bottom:-35px;">
    <marquee>
        <h2 style="color:darkgreen">Great Offer!! Special Batch is going be started very soon with 20% off!</h2>
    </marquee>
</div>
<div class="col-md-6 course-content description-back-color">
    <h2 class="course-header">Course Description</h2>
    <div class="text-justify">
        In recent years, the web has been getting more and more ingrained in our daily lives. We use it for everything, from shopping, to banking, to reading our news. The demand for web development talent is as high as ever, and the scope of what developers can do is huge!
        <p><a href="#"><img class="size-full wp-image-16467 aligncenter" src="http://ahmad-h.com/assets/img/website-development.jpg" alt="androido" width="394" height="240"></a></p>
        <p>Web programming, also known as web development, is the creation of dynamic web applications. Examples of web applications are social networking sites like Facebook or e-commerce sites like Amazon. The good news is that learning web development is not that hard!.</p>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2 class="panel-title">LEARNING OUTCOMES</h2>
            </div>
            <div class="panel-body">
                <p>At the end of this course, you will be able to:</p>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">Develop a complete Dynamic Web app leveraging different APIs</li>
                    <li class="list-group-item list-group-item-info">Deploy of app to Live Server</li>
                    <li class="list-group-item list-group-item-warning">Explore Web programming languages</li>
                    <li class="list-group-item list-group-item-danger">API Developmet for Mobile Apps</li>
                </ul>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h2 class="panel-title">LEARNING METHODOLOGY</h2>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success">Learning by Doing</li>
                    <li class="list-group-item list-group-item-info">Powerpoint Presentation</li>
                    <li class="list-group-item list-group-item-warning">Quiz</li>
                    <li class="list-group-item list-group-item-danger">Interactive Lecture</li>
                    <li class="list-group-item">Question and answer session</li>
                </ul>
            </div>
        </div>
        <h2>TRAINING SESSIONS AND CONTENT</h2>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active text-center col-md-6"><a href="#php" aria-controls="php" role="tab" data-toggle="tab">PHP</a></li>
            <li role="presentation" class="col-md-6 text-center"><a href="#mysql" aria-controls="mysql" role="tab" data-toggle="tab">MySQL</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="php">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 1: BASIC PHP</h3>
                    </div>
                    <div class="panel-body">
                        DATA TYPES,VARIABLE,OPERATORS,CONTANTS,STRINGS,CONITIONAL STATEMENT,ITERATIVE
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 2: BASIC PHP-2</h3>
                    </div>
                    <div class="panel-body">
                        LOOP,OPERATORS,FUNCTION,ARRAYS,FILES,DATE TIME,INCLUDE,REQUIRE,implode,explode
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 3: BASIC PHP-3</h3>
                    </div>
                    <div class="panel-body">
                        GET/POST,SESSION,COOCKIES,FILE UPLOAD,ERROR HADLING,REG EXP
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 4: OOP PHP</h3>
                    </div>
                    <div class="panel-body">
                        CLASS,OBJECT,CONSTRUCTOR,DESTRUCTOR,GETTER,SETTER,MAGIC METHODS
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 5: OOP PHP-2</h3>
                    </div>
                    <div class="panel-body">
                        INHERITANCE,METHOD OVERLOADING,FUNCTION OVERLOADING,POLYMORPHISM,ABSRTACT CLASS,INTERFACE
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 6: CRUD PHP</h3>
                    </div>
                    <div class="panel-body">
                        CRUD USING RAW PHP
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 7: CRUD PHP-2</h3>
                    </div>
                    <div class="panel-body">
                        CRUD USING OOP
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 8: Server Communication-1</h3>
                    </div>
                    <div class="panel-body">
                        Connecting with PHP & MySQL
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 9: CRUD PHP-3</h3>
                    </div>
                    <div class="panel-body">
                        LOG IN/LOG OUT USING SESSION & FILE
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 10: Google Map & Place API</h3>
                    </div>
                    <div class="panel-body">
                        Display Map Object, Latitude & Longitude, Placing Map Marker, Finding Current Location, Getting
                        Address from Latitude & Longitude.
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 11: API DEVELOPMENT</h3>
                    </div>
                    <div class="panel-body">
                        API Develoment for different platform
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 12: Deploy live server</h3>
                    </div>
                    <div class="panel-body">
                        How to deploy the project into live or staging server. As well as version controll using (Git, SVN)
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="mysql">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 1: DDL,DCL,DML,DTL</h3>
                    </div>
                    <div class="panel-body">
                        DDL,DCL,DML,DTL
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 2: Query</h3>
                    </div>
                    <div class="panel-body">
                        Queries, subqueries
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 3: Query-2</h3>
                    </div>
                    <div class="panel-body">
                        sql joining
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lecture 4: Query-3</h3>
                    </div>
                    <div class="panel-body">
                        agrregate functions,group by,having,like wil cards,between and,NOT IN etc
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3 course-content details-back-color">
    <h2 class="course-header">Course Details</h2>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Time Schedule & Course Fee</div>
        <!-- Table -->
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Start Date</th>
                    <td>1<sup>st</sup> November 2017</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Last Date of Reg.</th>
                    <td>31<sup>st</sup> October 2017</td>
                </tr>
                <tr>
                    <th scope="row">Class Time</th>
                    <td>9:00 am 11:30 am</td>
                </tr>
                <tr>
                    <th scope="row">No of Classes</th>
                    <td>12</td>
                </tr>
                <tr>
                    <th scope="row">Class Schedule</th>
                    <td>Sat, Mon, Wed</td>
                </tr>
                <tr>
                    <th scope="row">Duration</th>
                    <td>72 Hours</td>
                </tr>
                <tr>
                    <th scope="row">Venue</th>
                    <td>R#3, H#384, Sonadanga R/A (2nd phase), Khulna</td>
                </tr>
                <tr>
                    <th scope="row">Contact</th>
                    <td>+8801724160299</td>
                </tr>
                <tr>
                    <th scope="row">Email Us</th>
                    <td>xor.solution@gmail.com</td>
                </tr>
               <!-- <tr>
                    <th scope="row">Reg. Fee</th>
                    <td><del>50K BDT</del>  <span> 40K BDT</span></td>
                </tr>-->
                </tbody>
            </table>
        </div>
    </div>
    <!-- <div class="col-md-3"> -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d919.5529918352966!2d89.54497182917606!3d22.819095235208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjLCsDQ5JzA4LjciTiA4OcKwMzInNDMuOSJF!5e0!3m2!1sen!2sbd!4v1462508516150" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
    <!-- </div> -->
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <h2 class="course-header">About The Trainer</h2>
    <div class="gdlr-lms-course-info-author-image">
        <!-- <img alt="" src="http://xorbd.com/files/team/1456920938434.jpg" width="400" height="350"> -->
        <?php echo $this->Html->image('raz.jpg', array('width' => '400', 'height' => '350'))?>
    </div>
    <div class="gdlr-lms-course-info">
        <div class="gdlr-lms-info"><span class="head">Instructor </span><span class="tail"><a href="#"> Raz Ahmed</a></span></div>
    </div>
				<span>
					<div class="gdlr-lms-course-info"></div>
				</span>
    <div class="gdlr-lms-course-info"></div>
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">We are providing</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item list-group-item-success">Professional Learning & Working Environment</li>
                <li class="list-group-item list-group-item-info">Great opportunity to be part of a Team</li>
                <li class="list-group-item list-group-item-warning">Free Internet (WiFi)</li>
                <li class="list-group-item list-group-item-danger">High Skilled & Friendly Trainer</li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Attention !</h3>
        </div>
        <div class="panel-body">
            <h5 style="color:#a94442" class="text-animate">Student's should have to carry their personal computer.</h5>
        </div>
    </div>
</div>
</div>
<div role="tabpanel" class="tab-pane" id="home">
<div class="col-md-12" style="margin-bottom:-35px;">
    <marquee>
        <h2 style="color:darkgreen">Great Offer!! Now only 7,000 BDT for 2 in 1 package (both Android & java Course)!</h2>
    </marquee>
</div>
<div class="col-md-6 course-content description-back-color">
<h2 class="course-header">Course Description</h2>
<div class="text-justify">
Android application development has been flourished these days with over $32 billion of market share. According to IDC, the global share of Android in Smartphones is going to peak in 2015! Here is a detailed forecast:
<p><a href="#"><img class="size-full wp-image-16467 aligncenter" src="http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido.gif" alt="androido" width="394" height="240" srcset="http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido-300x183.gif 300w, http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido.gif 394w" sizes="(max-width: 394px) 100vw, 394px"></a></p>
<p>The future clearly lies in mobile apps when industry will absorb thousand of android application developer to develop small to large android application .This course will build your first stair to develop a complete mobile app that will comply industry standard.</p>
<div class="panel panel-info">
    <div class="panel-heading">
        <h2 class="panel-title">LEARNING OUTCOMES</h2>
    </div>
    <div class="panel-body">
        <p>At the end of this course, you will be able to:</p>
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">Develop a complete app leveraging different APIs;</li>
            <li class="list-group-item list-group-item-info">Deploy of app to google play store;</li>
            <li class="list-group-item list-group-item-warning">Explore Java programming language;</li>
            <li class="list-group-item list-group-item-danger">Monetize app.</li>
        </ul>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        <h2 class="panel-title">LEARNING METHODOLOGY</h2>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">Learning by Doing</li>
            <li class="list-group-item list-group-item-info">Powerpoint Presentation</li>
            <li class="list-group-item list-group-item-warning">Quiz</li>
            <li class="list-group-item list-group-item-danger">Interactive Lecture</li>
            <li class="list-group-item">Question and answer session</li>
        </ul>
    </div>
</div>
<h2>TRAINING SESSIONS AND CONTENT</h2>
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active text-center col-md-6"><a href="#android" aria-controls="android" role="tab" data-toggle="tab">Android</a></li>
    <li role="presentation" class="col-md-6 text-center"><a href="#java" aria-controls="java" role="tab" data-toggle="tab">Java</a></li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="android">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 1: Android Overview-1</h3>
            </div>
            <div class="panel-body">
                History of Android, Why Android, Android Architecture Stack, Essential Building Blocks of an Android App
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 2: Android Overview-2</h3>
            </div>
            <div class="panel-body">
                Setting up Android Studio, Making first "Hello Android" App, Understanding a Project's File
                Structure, USB Debugging
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 3: Basics</h3>
            </div>
            <div class="panel-body">
                Gradle, Activity, Life cycle of an Activity, Intent, Event Listener
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 4: Designing User Interface-1</h3>
            </div>
            <div class="panel-body">
                Learning about UI components, Layout Types
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 5: Designing User Interface-2</h3>
            </div>
            <div class="panel-body">
                Size Qualifiers, Nine-patch Graphic Assets, Designing Complex UI, Making Custom Components
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 6: Data Management-1</h3>
            </div>
            <div class="panel-body">
                SharedPreference, File
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 7: Data Management-2</h3>
            </div>
            <div class="panel-body">
                SQLite Database
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 8: Server Communication-1</h3>
            </div>
            <div class="panel-body">
                Connecting with PHP & MySQL
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 9: Server Communication-2</h3>
            </div>
            <div class="panel-body">
                JSON Parsing, XML Parsing
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 10: Google Map & Place API</h3>
            </div>
            <div class="panel-body">
                Display Map Object, Latitude & Longitude, Placing Map Marker, Finding Current Location, Getting
                Address from Latitude & Longitude.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 11: Background service & Broadcast receiver</h3>
            </div>
            <div class="panel-body">
                Service Life Cycle, Service Types, Service Implementation, Creating a Broadcast Receiver,
                Registering Broadcast Receiver
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 12: Admob & Google Playstore Account</h3>
            </div>
            <div class="panel-body">
                Integrating Banner and Interstitial Ads, How to Get Admob Payment, Not to-do's with an Admob Account,
                Opning an Account in Playstore, Steps to Release an Android App to Playstore.
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="java">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 1: Java Overview</h3>
            </div>
            <div class="panel-body">
                Why Java, History of Java, JDK versions, Bytecode and JVM, IDE, Basic syntax
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 2: Basics</h3>
            </div>
            <div class="panel-body">
                Identifiers, Variables, Constants, Data types, Operators, Type casting, Naming conventions.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 3: Decision Making and Loop</h3>
            </div>
            <div class="panel-body">
                If-else, Switch case, while, do-while, for loop
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 4: Arrays and Methods</h3>
            </div>
            <div class="panel-body">
                Array creation and initialization, Array processing, Multi-dimentional array, Defining custom
                methods, Method parameter, Method overloading
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 5: Strings</h3>
            </div>
            <div class="panel-body">
                String comparison, indexing, concatenation, substrings
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 6: OOP-1</h3>
            </div>
            <div class="panel-body">
                Object & Class, Constructor, Access Modifier, Encapsulation
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 7: OOP-2</h3>
            </div>
            <div class="panel-body">
                Inheritance & Polymorphism
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 8: OOP-3</h3>
            </div>
            <div class="panel-body">
                Abstract class & Interface
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 9: Exception & Multithreading</h3>
            </div>
            <div class="panel-body">
                Exception handling, types, declaration, throwing & catching exceptions, create custom exceptions
                Thread concept, Runnable interface, Thread states.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 10: QA</h3>
            </div>
            <div class="panel-body">
                Questions and Answers session about java.
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-md-3 course-content details-back-color">
    <h2 class="course-header">Course Details</h2>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active text-center col-md-6"><a href="#android-price" aria-controls="android-price" role="tab" data-toggle="tab">Android</a></li>
        <li role="presentation" class="col-md-6 text-center"><a href="#java-price" aria-controls="java-price" role="tab" data-toggle="tab">Java</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="android-price">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Time Schedule & Course Fee</div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Start Date</th>
                            <td>1 June 2016</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Last Date of Reg.</th>
                            <td>31 May 2016</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Time</th>
                            <td>4:00 pm-6:30 pm</td>
                        </tr>
                        <tr>
                            <th scope="row">No of Classes</th>
                            <td>12</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Schedule</th>
                            <td>Sat, Sun, Wed</td>
                        </tr>
                        <tr>
                            <th scope="row">Duration</th>
                            <td>30 Hours</td>
                        </tr>
                        <tr>
                            <th scope="row">Venue</th>
                            <td>R#3, H#384, Sonadanga R/A (2nd phase), Khulna</td>
                        </tr>
                        <tr>
                            <th scope="row">Contact</th>
                            <td>+8801724160299</td>
                        </tr>
                        <tr>
                            <th scope="row">Email Us</th>
                            <td>xor.solution@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Reg. Fee</th>
                            <td><del>6,000 BDT</del>  <span> 5,000 BDT</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- <div class="col-md-3"> -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d919.5529918352966!2d89.54497182917606!3d22.819095235208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjLCsDQ5JzA4LjciTiA4OcKwMzInNDMuOSJF!5e0!3m2!1sen!2sbd!4v1462508516150" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- </div> -->
        </div>
        <div role="tabpanel" class="tab-pane" id="java-price">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Time Schedule & Course Fee</div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Start Date</th>
                            <td>1 June 2016</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Last Date of Reg.</th>
                            <td>31 May 2016</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Time</th>
                            <td>10:00 am-12:30 pm</td>
                        </tr>
                        <tr>
                            <th scope="row">No of Classes</th>
                            <td>10</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Schedule</th>
                            <td>Sat, Sun, Wed</td>
                        </tr>
                        <tr>
                            <th scope="row">Duration</th>
                            <td>25 Hours</td>
                        </tr>
                        <tr>
                            <th scope="row">Venue</th>
                            <td>R#3, H#384, Sonadanga R/A (2nd phase), Khulna</td>
                        </tr>
                        <tr>
                            <th scope="row">Contact</th>
                            <td>+8801724160299</td>
                        </tr>
                        <tr>
                            <th scope="row">Email Us</th>
                            <td>xor.solution@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Reg. Fee</th>
                            <td><del>5,000 BDT</del>  <span> 4,000 BDT</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <h2 class="course-header">About The Trainer</h2>
    <div class="gdlr-lms-course-info-author-image">
        <!-- <img alt="" src="http://xorbd.com/files/team/1456920938434.jpg" width="400" height="350"> -->
        <?php echo $this->Html->image('shuvro.jpg', array('width' => '400', 'height' => '350'))?>
    </div>
    <div class="gdlr-lms-course-info">
        <div class="gdlr-lms-info"><span class="head">Instructor </span><span class="tail"><a href="#"> Shuvro Pal</a></span></div>
    </div>
				<span>
					<div class="gdlr-lms-course-info"></div>
				</span>
    <div class="gdlr-lms-course-info"></div>
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">We are providing</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item list-group-item-success">Professional Learning & Working Environment</li>
                <li class="list-group-item list-group-item-info">Great opportunity to be part of a Team</li>
                <li class="list-group-item list-group-item-warning">Free Internet (WiFi)</li>
                <li class="list-group-item list-group-item-danger">High Skilled & Friendly Trainer</li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Attention !</h3>
        </div>
        <div class="panel-body">
            <h5 style="color:#a94442" class="text-animate">Student's should have to carry their personal computer.</h5>
        </div>
    </div>
</div>

</div>
<div role="tabpanel" class="tab-pane" id="messages">
<div class="col-md-12" style="margin-bottom:-35px;">
    <marquee>
        <h2 style="color:darkgreen">Great Offer!! Now only 7,000 BDT for 2 in 1 package (both Android & java Course)!</h2>
    </marquee>
</div>
<div class="col-md-6 course-content description-back-color">
<h2 class="course-header">Course Description</h2>
<div class="text-justify">
Android application development has been flourished these days with over $32 billion of market share. According to IDC, the global share of Android in Smartphones is going to peak in 2015! Here is a detailed forecast:
<p><a href="#"><img class="size-full wp-image-16467 aligncenter" src="http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido.gif" alt="androido" width="394" height="240" srcset="http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido-300x183.gif 300w, http://mlab.mcc.com.bd/wp-content/uploads/2015/11/androido.gif 394w" sizes="(max-width: 394px) 100vw, 394px"></a></p>
<p>The future clearly lies in mobile apps when industry will absorb thousand of android application developer to develop small to large android application .This course will build your first stair to develop a complete mobile app that will comply industry standard.</p>
<div class="panel panel-info">
    <div class="panel-heading">
        <h2 class="panel-title">LEARNING OUTCOMES</h2>
    </div>
    <div class="panel-body">
        <p>At the end of this course, you will be able to:</p>
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">Develop a complete app leveraging different APIs;</li>
            <li class="list-group-item list-group-item-info">Deploy of app to google play store;</li>
            <li class="list-group-item list-group-item-warning">Explore Java programming language;</li>
            <li class="list-group-item list-group-item-danger">Monetize app.</li>
        </ul>
    </div>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        <h2 class="panel-title">LEARNING METHODOLOGY</h2>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">Learning by Doing</li>
            <li class="list-group-item list-group-item-info">Powerpoint Presentation</li>
            <li class="list-group-item list-group-item-warning">Quiz</li>
            <li class="list-group-item list-group-item-danger">Interactive Lecture</li>
            <li class="list-group-item">Question and answer session</li>
        </ul>
    </div>
</div>
<h2>TRAINING SESSIONS AND CONTENT</h2>
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active text-center col-md-6"><a href="#android" aria-controls="android" role="tab" data-toggle="tab">Android</a></li>
    <li role="presentation" class="col-md-6 text-center"><a href="#java" aria-controls="java" role="tab" data-toggle="tab">Java</a></li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="android">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 1: Android Overview-1</h3>
            </div>
            <div class="panel-body">
                History of Android, Why Android, Android Architecture Stack, Essential Building Blocks of an Android App
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 2: Android Overview-2</h3>
            </div>
            <div class="panel-body">
                Setting up Android Studio, Making first "Hello Android" App, Understanding a Project's File
                Structure, USB Debugging
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 3: Basics</h3>
            </div>
            <div class="panel-body">
                Gradle, Activity, Life cycle of an Activity, Intent, Event Listener
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 4: Designing User Interface-1</h3>
            </div>
            <div class="panel-body">
                Learning about UI components, Layout Types
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 5: Designing User Interface-2</h3>
            </div>
            <div class="panel-body">
                Size Qualifiers, Nine-patch Graphic Assets, Designing Complex UI, Making Custom Components
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 6: Data Management-1</h3>
            </div>
            <div class="panel-body">
                SharedPreference, File
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 7: Data Management-2</h3>
            </div>
            <div class="panel-body">
                SQLite Database
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 8: Server Communication-1</h3>
            </div>
            <div class="panel-body">
                Connecting with PHP & MySQL
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 9: Server Communication-2</h3>
            </div>
            <div class="panel-body">
                JSON Parsing, XML Parsing
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 10: Google Map & Place API</h3>
            </div>
            <div class="panel-body">
                Display Map Object, Latitude & Longitude, Placing Map Marker, Finding Current Location, Getting
                Address from Latitude & Longitude.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 11: Background service & Broadcast receiver</h3>
            </div>
            <div class="panel-body">
                Service Life Cycle, Service Types, Service Implementation, Creating a Broadcast Receiver,
                Registering Broadcast Receiver
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 12: Admob & Google Playstore Account</h3>
            </div>
            <div class="panel-body">
                Integrating Banner and Interstitial Ads, How to Get Admob Payment, Not to-do's with an Admob Account,
                Opning an Account in Playstore, Steps to Release an Android App to Playstore.
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="java">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 1: Java Overview</h3>
            </div>
            <div class="panel-body">
                Why Java, History of Java, JDK versions, Bytecode and JVM, IDE, Basic syntax
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 2: Basics</h3>
            </div>
            <div class="panel-body">
                Identifiers, Variables, Constants, Data types, Operators, Type casting, Naming conventions.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 3: Decision Making and Loop</h3>
            </div>
            <div class="panel-body">
                If-else, Switch case, while, do-while, for loop
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 4: Arrays and Methods</h3>
            </div>
            <div class="panel-body">
                Array creation and initialization, Array processing, Multi-dimentional array, Defining custom
                methods, Method parameter, Method overloading
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 5: Strings</h3>
            </div>
            <div class="panel-body">
                String comparison, indexing, concatenation, substrings
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 6: OOP-1</h3>
            </div>
            <div class="panel-body">
                Object & Class, Constructor, Access Modifier, Encapsulation
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 7: OOP-2</h3>
            </div>
            <div class="panel-body">
                Inheritance & Polymorphism
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 8: OOP-3</h3>
            </div>
            <div class="panel-body">
                Abstract class & Interface
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 9: Exception & Multithreading</h3>
            </div>
            <div class="panel-body">
                Exception handling, types, declaration, throwing & catching exceptions, create custom exceptions
                Thread concept, Runnable interface, Thread states.
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Lecture 10: QA</h3>
            </div>
            <div class="panel-body">
                Questions and Answers session about java.
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-md-3 course-content details-back-color">
    <h2 class="course-header">Course Details</h2>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active text-center col-md-6"><a href="#android-price" aria-controls="android-price" role="tab" data-toggle="tab">Android</a></li>
        <li role="presentation" class="col-md-6 text-center"><a href="#java-price" aria-controls="java-price" role="tab" data-toggle="tab">Java</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="android-price">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Time Schedule & Course Fee</div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Start Date</th>
                            <td>1 June 2016</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Last Date of Reg.</th>
                            <td>31 May 2016</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Time</th>
                            <td>4:00 pm-6:30 pm</td>
                        </tr>
                        <tr>
                            <th scope="row">No of Classes</th>
                            <td>12</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Schedule</th>
                            <td>Sat, Sun, Wed</td>
                        </tr>
                        <tr>
                            <th scope="row">Duration</th>
                            <td>30 Hours</td>
                        </tr>
                        <tr>
                            <th scope="row">Venue</th>
                            <td>R#3, H#384, Sonadanga R/A (2nd phase), Khulna</td>
                        </tr>
                        <tr>
                            <th scope="row">Contact</th>
                            <td>+8801724160299</td>
                        </tr>
                        <tr>
                            <th scope="row">Email Us</th>
                            <td>xor.solution@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Reg. Fee</th>
                            <td><del>6,000 BDT</del>  <span> 5,000 BDT</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- <div class="col-md-3"> -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d919.5529918352966!2d89.54497182917606!3d22.819095235208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjLCsDQ5JzA4LjciTiA4OcKwMzInNDMuOSJF!5e0!3m2!1sen!2sbd!4v1462508516150" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- </div> -->
        </div>
        <div role="tabpanel" class="tab-pane" id="java-price">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Time Schedule & Course Fee</div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Start Date</th>
                            <td>1 June 2016</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Last Date of Reg.</th>
                            <td>31 May 2016</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Time</th>
                            <td>10:00 am-12:30 pm</td>
                        </tr>
                        <tr>
                            <th scope="row">No of Classes</th>
                            <td>10</td>
                        </tr>
                        <tr>
                            <th scope="row">Class Schedule</th>
                            <td>Sat, Sun, Wed</td>
                        </tr>
                        <tr>
                            <th scope="row">Duration</th>
                            <td>25 Hours</td>
                        </tr>
                        <tr>
                            <th scope="row">Venue</th>
                            <td>R#3, H#384, Sonadanga R/A (2nd phase), Khulna</td>
                        </tr>
                        <tr>
                            <th scope="row">Contact</th>
                            <td>+8801724160299</td>
                        </tr>
                        <tr>
                            <th scope="row">Email Us</th>
                            <td>xor.solution@gmail.com</td>
                        </tr>
                        <tr>
                            <th scope="row">Reg. Fee</th>
                            <td><del>5,000 BDT</del>  <span> 4,000 BDT</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <h2 class="course-header">About The Trainer</h2>
    <div class="gdlr-lms-course-info-author-image">
        <!-- <img alt="" src="http://xorbd.com/files/team/1456920938434.jpg" width="400" height="350"> -->
        <?php echo $this->Html->image('shuvro.jpg', array('width' => '400', 'height' => '350'))?>
    </div>
    <div class="gdlr-lms-course-info">
        <div class="gdlr-lms-info"><span class="head">Instructor </span><span class="tail"><a href="#"> Shuvro Pal</a></span></div>
    </div>
				<span>
					<div class="gdlr-lms-course-info"></div>
				</span>
    <div class="gdlr-lms-course-info"></div>
</div>
<div class="col-md-3 course-content about-trainer-back-color">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">We are providing</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item list-group-item-success">Professional Learning & Working Environment</li>
                <li class="list-group-item list-group-item-info">Great opportunity to be part of a Team</li>
                <li class="list-group-item list-group-item-warning">Free Internet (WiFi)</li>
                <li class="list-group-item list-group-item-danger">High Skilled & Friendly Trainer</li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Attention !</h3>
        </div>
        <div class="panel-body">
            <h5 style="color:#a94442" class="text-animate">Student's should have to carry their personal computer.</h5>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
    .header_large{
        position: relative;
    }
    .text-animate{
        /*width: 100px;*/
        /*height: 100px;*/
        /*background-color: red;*/
        position: relative;
        -webkit-animation-name: example; /* Chrome, Safari, Opera */
        -webkit-animation-duration: 4s; /* Chrome, Safari, Opera */
        -webkit-animation-iteration-count: infinite; /* Chrome, Safari, Opera */
        animation-name: example;
        animation-duration: 4s;
        animation-iteration-count: infinite;
    }
    /* Chrome, Safari, Opera */
    @-webkit-keyframes example {
        0%   {color:red; left:0px; top:0px;}
        25%  {color:yellow; right:200px; top:200px;}
        50%  {color:blue; right:0px; top:0px;}
        75%  {color:green; right:200px; top:0px;}
        100% {color:red; left:0px; top:0px;}
    }
    /* Standard syntax */
    /*@keyframes example {
    0%   {background-color:red; left:0px; top:0px;}
    25%  {background-color:yellow; left:200px; top:0px;}
    50%  {background-color:blue; left:200px; top:200px;}
    75%  {background-color:green; left:0px; top:200px;}
    100% {background-color:red; left:0px; top:0px;}
    }*/
    #map {
        height: 300px;
        width: 300px;
    }
</style>