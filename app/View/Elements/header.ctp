
<header id="header" class="header-scrolled">
    <div class="container-fluid">

        <div id="logo" class="pull-left">
            <a href="#intro"><img src="img/logo.png" width="145px" alt="|XOR Software Solution" title="" class="js-big"/></a>
            <a href="#intro"><img src="img/logo.png" width="75px" alt="|XOR Software Solution" title="XOR Software Solution" class="js-small" style="display: none;"/></a>
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <?php foreach ($menues as $key => $menue) {?>

                    <li class="">
                        <a href="<?php echo $this->Html->url($menue['Menu']['link'])?>" class=""><?php echo $menue['Menu']['name'];
                            ?>
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </nav>
    </div>
</header>
