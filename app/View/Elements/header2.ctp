    <script type="text/javascript">
        $(document).ready(function(){

            // This initialises carousels on the container elements specified, in this case, carousel1.
            $("#carousel1").CloudCarousel(
                {
                    autoRotate: 'yes',
                    reflHeight: 40,
                    reflGap:2,
                    titleBox: $('#da-vinci-title'),
                    altBox: $('#da-vinci-alt'),
                    buttonLeft: $('#slider-left-but'),
                    buttonRight: $('#slider-right-but'),
                    yRadius:50,
                    xPos: 480,
                    yPos: 32,
                    speed:0.15
                }
            );
            //$("#content").removeAttr("id");
        });

    </script>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=181256498737365";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="container">
    <div id="templatemo_header_wrapper">
        <div id="site_title">
            <span class="title">Dream Boy</span><div class="cleaner"></div><span class="quote">Think Exceptional, be Exception</span>
        </div>

        <div class="cell">
            <div class="country_icon"></div>
            <span class="cell_no">+8801724-160299</span>
        </div>
        <div class="cleaner"></div>
        <div class="menu">
            <div id="templatemo_menu" class="ddsmoothmenu">
                <ul>
                    <li class="active"><a href="<?php echo $this->Html->url('/')?>" class="selected">Home</a></li><div class="border_right"></div>
                    <li class="active"><a href="#">Galary</a>
                        <ul>
                            <li><span class="top"></span><span class="bottom"></span></li>
                            <li><a href="http://www.templatemo.com/page/1">Sub menu 1</a></li>
                            <li><a href="http://www.templatemo.com/page/2">Sub menu 2</a></li>
                            <li><a href="http://www.templatemo.com/page/3">Sub menu 3</a></li>
                        </ul>
                    </li><div class="border_right"></div>

                    <li class="active"><a href="#">Projects</a>
                        <ul>
                            <li><span class="top"></span><span class="bottom"></span></li>
                            <li><a href="http://www.templatemo.com/page/1">Ongoing Project</a></li>
                            <li><a href="http://www.templatemo.com/page/2">Complited Project</a></li>
                        </ul>
                    </li><div class="border_right"></div>
                    <li class="active"><a href="#">Activities</a>
                        <ul>
                            <li><span class="top"></span><span class="bottom"></span></li>
                            <li><a href="http://www.templatemo.com/page/1">Website Development</a></li>
                            <li><a href="http://www.templatemo.com/page/2">Study</a></li>
                            <li><a href="http://www.templatemo.com/page/3">Advertisement</a></li>
                            <li><a href="http://www.templatemo.com/page/4">Sub menu 4</a></li>
                            <li><a href="http://www.templatemo.com/page/5">Sub menu 5</a></li>
                        </ul>
                    </li><div class="border_right"></div>
                    <li class="active"><a href="<?php echo $this->Html->url('/site/about')?>">About</a></li><div class="border_right"></div>
                    <li class="active"><a href="<?php echo $this->Html->url('/site/about')?>">Contact</a></li>
                </ul>
                <br style="clear: left" />
            </div> <!-- end of templatemo_menu -->
        </div>
        <div class="cleaner"></div>
    </div>	<!-- END of templatemo_header_wrapper -->
    <?=$content?>
    <div id="footer">
        <div id="templatemo_bottom_wrapper">
            <div id="templatemo_bottom">
                <div class="col one_third">
                    <h4><span></span>Activities</h4>
                    <div class="bottom_box">
                        <ul class="footer_list">
                            <li><a href="http://www.templatemo.com/page/1">Project's</a></li>
                            <li><a href="http://www.webdesignmo.com/blog">Sports</a></li>
                            <li><a href="http://www.flashmo.com">Tour</a></li>
                            <li><a href="http://www.templatemo.com">Website Templates</a></li>
                            <li><a href="http://www.koflash.com">Free Web Gallery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col one_third">
                    <h4><span></span>Projects</h4>
                    <div class="bottom_box">
                        <ul class="twitter_post">
                            <li>weareamigos creates connection among people for sharing their interest, activities, behavior and their real life experience by making friendship.</li>
                            <li>Proin dignissim, diam nec <a href="#">@TemplateMo</a> enim lorem tempus orci, ac imperdiet ante purus in justo.</li>
                        </ul>
                    </div>
                </div>
                <div class="col one_third no_margin_right">
                    <h4><span></span>About</h4>
                    <div class="bottom_box">
                        <p><em>In 5 march 1990, I live in a middle class family in Jessore District. In childhood, when the the plane fly in the sky, I wish to be a pilot. When the poor people suffer with sickness, I wish to be a doctor. But change with time, now I'm a software Engineer. Web Development is my passion.</em></p><br />
                        <div class="footer_social_button">
                            <a href="#"><img src="images/facebook.png" title="facebook" alt="facebook" /></a>
                            <a href="#"><img src="images/flickr.png" title="flickr" alt="flickr" /></a>
                            <a href="#"><img src="images/twitter.png" title="twitter" alt="twitter" /></a>
                            <a href="#"><img src="images/youtube.png" title="youtube" alt="youtube" /></a>
                            <a href="#"><img src="images/feed.png" title="rss" alt="rss" /></a>
                        </div>
                    </div>
                </div>

                <div class="cleaner"></div>
            </div> <!-- END of tempatemo_bottom -->
        </div> <!-- END of tempatemo_bottom_wrapper -->
        <div id="templatemo_footer_wrapper">
            <div id="templatemo_footer">
                Copyright © 2013| Designed by Raaz
            </div> <!-- END of templatemo_footer_wrapper -->
        </div> <!-- END of templatemo_footer -->
</div>	<!-- END of CONTAINER -->
</body>
</html>