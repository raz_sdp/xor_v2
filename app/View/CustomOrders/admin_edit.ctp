<div class="customOrders form">
<?php echo $this->Form->create('CustomOrder'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Custom Order'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('project_type');
		echo $this->Form->input('order_date');
		echo $this->Form->input('delivery_date');
		echo $this->Form->input('requirements_link');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CustomOrder.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('CustomOrder.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Custom Orders'), array('action' => 'index')); ?></li>
	</ul>
</div>
