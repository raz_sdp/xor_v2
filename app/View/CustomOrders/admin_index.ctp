<div class="customOrders index">
	<h2><?php echo __('Custom Orders'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_type'); ?></th>
			<th><?php echo $this->Paginator->sort('order_date'); ?></th>
			<th><?php echo $this->Paginator->sort('delivery_date'); ?></th>
			<th><?php echo $this->Paginator->sort('requirements_link'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($customOrders as $customOrder): ?>
	<tr>
		<td><?php echo h($customOrder['CustomOrder']['id']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['customer_id']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['project_type']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['order_date']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['delivery_date']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['requirements_link']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['created']); ?>&nbsp;</td>
		<td><?php echo h($customOrder['CustomOrder']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $customOrder['CustomOrder']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customOrder['CustomOrder']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customOrder['CustomOrder']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $customOrder['CustomOrder']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Custom Order'), array('action' => 'add')); ?></li>
	</ul>
</div>
