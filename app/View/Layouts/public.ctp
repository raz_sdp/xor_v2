<?php
$cakeDescription = __d('cake_dev', 'XOR ');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
	</title>
    <!-- responsive meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript">
        var ROOT = '<?php echo $this->Html->url('/',true); ?>';
    </script>
   <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,700,500,300' rel='stylesheet' type='text/css'>
    <link href="fonts/flaticon/flaticon.css" rel="stylesheet">
    <script src="js/vendor/modernizr-2.8.1.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>-->
    <?php
        echo $this->Html->meta('icon');
        //echo $this->Html->css('cake.generic');        
        echo $this->Html->css(array(
            'bootstrap4.min','style2',
           'font-awesome.min','animate.min','ionicons.min','owl.carousel.css','lightbox.min'
        ));
        echo $this->Html->script(array(
//            'jquery-2.1.1.min',
            'jquery.min',
            /*'myblog', 'jquery', 'wow.min', 'flickerPhoto.min',
            'jquery.shuffle.min', 'owl.carousel.min', /*'hippo-offcanvas',*/
            /*'twitterFetcher_min',
            'jquery.inview.min', 'jquery.stellar', 'jquery.countTo','scripts',*/
            'wow.min',
            'bootstrap.min',
            'main',
            'jquery-migrate.min',
            'bootstrap.bundle.min',
            'easing.min',
            'hoverIntent',
            'superfish.min',
            'wow.min',
            'waypoints.min',
            'counterup.min',
            'owl.carousel.min',
            'isotope.pkgd.min',
            'lightbox.min',
            'jquery.touchSwipe.min',
            'contactform'
        ));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body id="page-top" data-spy="scroll"  data-target=".navbar">

        <?php echo $this->element('header'); ?>
		<?php //echo $this->element('new-header'); ?>

        <div id="content">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>

        <div id="footer">
            <!--<div style="background:black;color:white;text-align:center;height:40px;margin-top:30px">sdfsdaf</div>-->           
        </div>

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>