<div class="sliderLayers index">
	<h2><?php echo __('Slider Layers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('slider_image_id'); ?></th>
			<th><?php echo $this->Paginator->sort('order'); ?></th>
			<th><?php echo $this->Paginator->sort('class'); ?></th>
			<th><?php echo $this->Paginator->sort('data-x'); ?></th>
			<th><?php echo $this->Paginator->sort('data-hoffset'); ?></th>
			<th><?php echo $this->Paginator->sort('data-y'); ?></th>
			<th><?php echo $this->Paginator->sort('data-voffset'); ?></th>
			<th><?php echo $this->Paginator->sort('data-customin'); ?></th>
			<th><?php echo $this->Paginator->sort('data-customout'); ?></th>
			<th><?php echo $this->Paginator->sort('data-speed'); ?></th>
			<th><?php echo $this->Paginator->sort('data-start'); ?></th>
			<th><?php echo $this->Paginator->sort('data-easing'); ?></th>
			<th><?php echo $this->Paginator->sort('data-endspeed'); ?></th>
			<th><?php echo $this->Paginator->sort('data-end'); ?></th>
			<th><?php echo $this->Paginator->sort('data-endeasing'); ?></th>
			<th><?php echo $this->Paginator->sort('data-captionhidden'); ?></th>
			<th><?php echo $this->Paginator->sort('content_type'); ?></th>
			<th><?php echo $this->Paginator->sort('text'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('image_width'); ?></th>
			<th><?php echo $this->Paginator->sort('image_height'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sliderLayers as $sliderLayer): ?>
	<tr>
		<td><?php echo h($sliderLayer['SliderLayer']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($sliderLayer['SliderImage']['id'], array('controller' => 'slider_images', 'action' => 'view', $sliderLayer['SliderImage']['id'])); ?>
		</td>
		<td><?php echo h($sliderLayer['SliderLayer']['order']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['class']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-x']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-hoffset']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-y']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-voffset']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-customin']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-customout']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-speed']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-start']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-easing']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-endspeed']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-end']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-endeasing']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['data-captionhidden']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['content_type']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['text']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['image']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['image_width']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['image_height']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['created']); ?>&nbsp;</td>
		<td><?php echo h($sliderLayer['SliderLayer']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sliderLayer['SliderLayer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sliderLayer['SliderLayer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sliderLayer['SliderLayer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $sliderLayer['SliderLayer']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Slider Layer'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('controller' => 'slider_images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Image'), array('controller' => 'slider_images', 'action' => 'add')); ?> </li>
	</ul>
</div>
