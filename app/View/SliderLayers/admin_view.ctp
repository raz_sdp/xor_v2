<div class="sliderLayers view">
<h2><?php echo __('Slider Layer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slider Image'); ?></dt>
		<dd>
			<?php echo $this->Html->link($sliderLayer['SliderImage']['id'], array('controller' => 'slider_images', 'action' => 'view', $sliderLayer['SliderImage']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Class'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['class']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-x'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-x']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-hoffset'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-hoffset']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-y'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-y']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-voffset'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-voffset']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-customin'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-customin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-customout'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-customout']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-speed'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-speed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-start'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-easing'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-easing']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-endspeed'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-endspeed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-end'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-endeasing'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-endeasing']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Data-captionhidden'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['data-captionhidden']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content Type'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['content_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Text'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image Width'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['image_width']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image Height'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['image_height']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($sliderLayer['SliderLayer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slider Layer'), array('action' => 'edit', $sliderLayer['SliderLayer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slider Layer'), array('action' => 'delete', $sliderLayer['SliderLayer']['id']), array(), __('Are you sure you want to delete # %s?', $sliderLayer['SliderLayer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Slider Layers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Layer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('controller' => 'slider_images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Image'), array('controller' => 'slider_images', 'action' => 'add')); ?> </li>
	</ul>
</div>
