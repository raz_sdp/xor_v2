<div class="sliderLayers form">
<?php echo $this->Form->create('SliderLayer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Slider Layer'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('slider_image_id');
		echo $this->Form->input('order');
		echo $this->Form->input('class');
		echo $this->Form->input('data-x');
		echo $this->Form->input('data-hoffset');
		echo $this->Form->input('data-y');
		echo $this->Form->input('data-voffset');
		echo $this->Form->input('data-customin');
		echo $this->Form->input('data-customout');
		echo $this->Form->input('data-speed');
		echo $this->Form->input('data-start');
		echo $this->Form->input('data-easing');
		echo $this->Form->input('data-endspeed');
		echo $this->Form->input('data-end');
		echo $this->Form->input('data-endeasing');
		echo $this->Form->input('data-captionhidden');
		echo $this->Form->input('content_type');
		echo $this->Form->input('text');
		echo $this->Form->input('image');
		echo $this->Form->input('image_width');
		echo $this->Form->input('image_height');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SliderLayer.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SliderLayer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Layers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('controller' => 'slider_images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Image'), array('controller' => 'slider_images', 'action' => 'add')); ?> </li>
	</ul>
</div>
