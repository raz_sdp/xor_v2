<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="sliderImages form">
<?php echo $this->Form->create('SliderImage', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Slider Image'); ?></legend>
	<?php
        if(!empty($this->request->data['SliderImage']['image_link'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/' . $this->request->data['SliderImage']['image_link'], array('alt'=>'picture', 'width'=> '150px','height' => '150px'));
            echo "</div>";
        }
        echo $this->Form->input('image_link', array('type' => 'file'));
		echo $this->Form->input('order');
    ?>
        <div class="input text">
            <label for="TransitionType">Transition Type</label>
            <select name="data[SliderImage][transition_type]">
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "fade" ) echo "selected";?> value="fade">Fade</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "zoomin" ) echo "selected";?> value="zoomin">Zoom In</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "zoomout" ) echo "selected";?> value="zoomout">Zoom Out</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "slideup" ) echo "selected";?> value="slideup">Slide Up</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "slidedown" ) echo "selected";?> value="slidedown">Slide Down</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "slideleft" ) echo "selected";?> value="slideleft">Slide Left</option>
                <option <?php if( $this->request->data['SliderImage']['transition_type'] == "slideright" ) echo "selected";?> value="slideright">Slide Right</option>
            </select>
        </div>
    <?php
        echo $this->Form->input('speed');
	?>
	</fieldset>

    <script>
        $('input[name="data[SliderImage][image_link]"]').on('change', function(){
            var new_img =  $('input[name="data[SliderImage][image_link]"]').val();
            $('.thumbnail-item').find('img').attr('src', new_img);
        });
    </script>

<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SliderImage.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SliderImage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('action' => 'index')); ?></li>
	</ul>
</div>
