<div class="sliderImages view">
<h2><?php echo __('Slider Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Background Type'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['background_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image Link'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['image_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($sliderImage['SliderImage']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slider Image'), array('action' => 'edit', $sliderImage['SliderImage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slider Image'), array('action' => 'delete', $sliderImage['SliderImage']['id']), array(), __('Are you sure you want to delete # %s?', $sliderImage['SliderImage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Image'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Slider Contents'), array('controller' => 'slider_contents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slider Content'), array('controller' => 'slider_contents', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Slider Contents'); ?></h3>
	<?php if (!empty($sliderImage['SliderContent'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Slider Image Id'); ?></th>
		<th><?php echo __('Content'); ?></th>
		<th><?php echo __('Transition'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($sliderImage['SliderContent'] as $sliderContent): ?>
		<tr>
			<td><?php echo $sliderContent['id']; ?></td>
			<td><?php echo $sliderContent['slider_image_id']; ?></td>
			<td><?php echo $sliderContent['content']; ?></td>
			<td><?php echo $sliderContent['transition']; ?></td>
			<td><?php echo $sliderContent['created']; ?></td>
			<td><?php echo $sliderContent['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'slider_contents', 'action' => 'view', $sliderContent['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'slider_contents', 'action' => 'edit', $sliderContent['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'slider_contents', 'action' => 'delete', $sliderContent['id']), array(), __('Are you sure you want to delete # %s?', $sliderContent['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Slider Content'), array('controller' => 'slider_contents', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
