<?php #AuthComponent::_setTrace($data); ?>
<div id="slider_preview" class="slider_preview" >
    <?php echo $this->Html->css(array('style','settings')); ?>
    <?php echo $this->Html->script(array('jquery', 'jquery.themepunch.tools.min', 'jquery.themepunch.revolution.min')); ?>

    <article class="boxedcontainer">

        <div class="tp-banner-container">
            <div class="tp-banner" >

                <ul>
                    <?php

                    foreach($data as $key => $slide){

                        echo "<li data-transition=\"". $slide['SliderImage']['transition_type'] . "\" data-slotamount=\"7\" data-masterspeed=\"" . $slide['SliderImage']['speed'] . "\" >";

                        $main_image = $slide['SliderImage']['image_link'];
                        echo $this->Html->image($main_image, array('alt'=>"", 'style'=>"background-color:#b2c4cc", 'data-bgfit'=>"cover", 'data-bgposition'=>"left top", 'data-bgrepeat'=>"no-repeat"));

                        foreach($slide['SliderLayer'] as $layer) {
                            echo "<div class=\"" . $layer['class'] . "\"" ;
                            if(!empty($layer['data-x'])) echo " data-x=\"". $layer['data-x'] . "\"";
                            if(!empty($layer['data-hoffset'])) echo " data-hoffset=\"". $layer['data-hoffset'] . "\"";
                            if(!empty($layer['data-y'])) echo " data-y=\"". $layer['data-y'] . "\"";
                            if(!empty($layer['data-voffset'])) echo " data-voffset=\"". $layer['data-voffset'] . "\"";
                            if(!empty($layer['data-customin'])) echo " data-customin=\"". $layer['data-customin'] . "\"";
                            if(!empty($layer['data-customout'])) echo " data-customout=\"". $layer['data-customout'] . "\"";
                            if(!empty($layer['data-speed'])) echo " data-speed=\"". $layer['data-speed'] . "\"";
                            if(!empty($layer['data-start'])) echo " data-start=\"". $layer['data-start'] . "\"";
                            if(!empty($layer['data-easing'])) echo " data-easing=\"". $layer['data-easing'] . "\"";
                            if(!empty($layer['data-endspeed'])) echo " data-endspeed=\"". $layer['data-endspeed'] . "\"";
                            if(!empty($layer['data-end'])) echo " data-end=\"". $layer['data-end'] . "\"";
                            if(!empty($layer['data-endeasing'])) echo " data-endeasing=\"". $layer['data-endeasing'] . "\"";
                            if(!empty($layer['data-captionhidden'])) echo " data-captionhidden=\"". $layer['data-captionhidden'] . "\"";
                            if($layer['content_type'] == 'vdo') {
                                echo " data-autoplay=\"false\"";
                                echo " data-autoplayonlyfirsttime=\"false\"";
                            }
                            $z = ($key == 0) ? ($layer['order'] + 2) : ($layer['order'] + 1) ;
                            echo " style=\"z-index: ". $z . ";\"";
                            echo " >";

                            if($layer['content_type'] == 'img') {
                                $image_options = array('alt'=>"");
                                if(!empty($layer['image_width']))
                                    $image_options['data-ww'] = $layer['image_width'];
                                if(!empty($layer['image_height']))
                                    $image_options['data-hh'] = $layer['image_height'];

                                echo $this->Html->image($layer['image'], $image_options);
                            } else {
                                echo $layer['text'];
                            }

                            echo "</div>";
                        }

                        echo "</li>";

                    }

                    ?>
                </ul>

                <div class="tp-bannertimer"></div>

                <button class="slider_close-btn" id="slider_close-btn">Close Preview</button>

            </div>
        </div>

        <h2 class="slider_navigation">
            <span class="link-arrow">
                <?php echo $this->Html->link(__('Reset Everything To Default'), array('action' => 'all_default' , 'admin'=>true));?>
            </span>
        </h2>

        <!-- THE SCRIPT INITIALISATION -->
        <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
        <script type="text/javascript">
            var revapi;
            jQuery(document).ready(function() {
                revapi = jQuery('.tp-banner').revolution(
                    {
                        delay: <?php echo $general['SliderGeneralSetting']['delay'];?>,
                        startwidth: <?php echo $general['SliderGeneralSetting']['start_width'];?>,
                        startheight: <?php echo $general['SliderGeneralSetting']['start_height'];?>,
                        hideThumbs: <?php echo $general['SliderGeneralSetting']['hide_thumbs'];?>,
                        fullWidth: "on",
                        forceFullWidth: "on"
                    });

                $('#slider_close-btn').click(function(){
                    location.href = 'general_settings';
                });
            });
        </script>

        <!-- END REVOLUTION SLIDER -->
    </article><!-- Content End -->

</div>