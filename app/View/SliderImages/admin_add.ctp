<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="sliderImages form">
<?php echo $this->Form->create('SliderImage', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Slider Image'); ?></legend>
	<?php
        if(!empty($this->request->data['SliderImage']['image_link'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/' . $this->request->data['SliderImage']['image_link'], array('alt'=>'picture', 'width'=> '150px','height' => '150px'));
            echo "</div>";
        }
        echo $this->Form->input('image_link', array('type' => 'file'));
		echo $this->Form->input('order');
    ?>
        <div class="input text">
            <label for="TransitionType">Transition Type</label>
            <select name="data[SliderImage][transition_type]">
                <option value="fade">Fade</option>
                <option value="zoomin">Zoom In</option>
                <option value="zoomout">Zoom Out</option>
                <option value="slideup">Slide Up</option>
                <option value="slidedown">Slide Down</option>
                <option value="slideleft">Slide Left</option>
                <option value="slideright">Slide Right</option>
            </select>
        </div>
    <?php
        echo $this->Form->input('speed');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

    <script>
        $('input[name="data[SliderImage][image_link]"]').on('change', function(){
            var new_img =  $('input[name="data[SliderImage][image_link]"]').val();
            $('.thumbnail-item').find('img').attr('src', new_img);
        });
    </script>

</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('action' => 'index')); ?></li>
    </ul>
</div>
