            <div id="grid">
                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "marketing", "development", "branding"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-1.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Design / Development</span>
                                    <h2><a href="#">Healthcare Textiles</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>
                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "marketing", "design", "branding"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-2.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Marketing / Branding</span>
                                    <h2><a href="#">Affiliate Marketing</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>

                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "development"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-3.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Design / Development</span>
                                    <h2><a href="#">Email Template Designing</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>

                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "design", "branding"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-4.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Development / Marketing</span>
                                    <h2><a href="#">Web Template Marketing</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>

                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "marketing", "design"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-5.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Branding / Marketing</span>
                                    <h2><a href="#">Product Promotion Branding</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>

                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "development", "branding"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <img src="img/work/portfolio-6.jpg" alt="">
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal1" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span> Design / Branding</span>
                                    <h2><a href="#">Product Promotion Branding</a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>
            </div>