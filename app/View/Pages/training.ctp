<div class="landing-page">
   <div class="row">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	         <!-- Indicators -->
	         <ol class="carousel-indicators">
	            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	         </ol>
	         <!-- Wrapper for slides -->
	         <div class="carousel-inner" role="listbox">
	            <!--<div class="item active">
	               <img src="<?php echo $this->Html->url('/files/training/course.jpg', true)?>" alt="Android_1">
	               <div class="carousel-caption">
	                  <h1 style="color:red;">Great Offer</h1>
	                  <p>Ongoing....</p>
	               </div>
	            </div>-->
	            <div class="item active">
	               <img src="<?php echo $this->Html->url('/files/training/android_1.jpg', true)?>" alt="Android_1">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src="<?php echo $this->Html->url('/files/training/java_1.jpg', true)?>" alt="Android_1">
	               <div class="carousel-caption">
	                 <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src=" <?php echo $this->Html->url('/files/training/ios_1.jpg', true)?>" alt="Android_2">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src=" <?php echo $this->Html->url('/files/training/web_1.jpg', true)?>" alt="Android_2">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src=" <?php echo $this->Html->url('/files/training/android_2.jpg', true)?>" alt="Android_2">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src=" <?php echo $this->Html->url('/files/training/ios_2.jpg', true)?>" alt="Android_2">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	            <div class="item">
	               <img src=" <?php echo $this->Html->url('/files/training/web_2.jpg', true)?>" alt="Android_2">
	               <div class="carousel-caption">
	                  <h1 class=""><a href="https://docs.google.com/forms/d/1Txbk24HpLSWzZbMgCieUR-vGe-4YElG7EBjlwG1t9fE/edit?usp=sharing_eid&ts=5729f0b4" target="_blank" class="btn btn-success btn-lg btn-block">Ongoing Admission Register Today.</a></h1>
	               </div>
	            </div>
	         </div>
	         <!-- Controls -->
	         <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	         <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	         <span class="sr-only">Previous</span>
	         </a>
	         <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	         <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	         <span class="sr-only">Next</span>
	         </a>
    	</div>
   </div>
   <?php echo $this->element('course-tab-content')?>
	<div class="col-md-12" style="padding:0px; margin-top:15px;">
	   <?php echo $this->element('footer')?>	
	</div>
</div>

<style type="text/css">
	.carousel-inner img{
		width: 100% !important;
	}
</style>