<style>
    .portfolio-img{
        max-width: 100%;
        height: 250px;
        width: 350px;
    }
</style>
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active" style="background-image: url('img/intro-carousel/1.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>WE ARE PROFESSIONAL</h2>

                            <p>
                                Our services are quick, easy, tailored, and secure and allow you to manage your business
                                reporting and transactions at a time that is convenient for you.</p>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('img/intro-carousel/2.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>CHANGE IS OPPORTUNITY</h2>

                            <p>We're a technologically forward thinking company, we embrace change, welcome it and
                                always look for the opportunity in every change we see happen; whether that be emerging
                                technology, shifting markets or consumer behaviour patterns, we seek opportunities to
                                give our customers competitive advantages and to further refine our business.
                            </p>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('img/intro-carousel/3.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>DELIVER WOW</h2>

                            <p>We're not happy with OK. We always aim to deliver WOW to our customers, whether that be
                                with our value for money, intuitive software, or with great customer service.</p>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('img/intro-carousel/4.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>BE CRAZY</h2>

                            <p>To innovate we need to be a little crazy, think out of the box and be agile. Don't be
                                afraid to fail - fail fast and learn quick, to find the best solutions for our customers
                                and business.</p>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('img/intro-carousel/5.jpg');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>HAPPY PEOPLE</h2>

                            <p>We are all one big family; founders, employees, contractors, suppliers and customers. To
                                be happy, we all need to be happy. Don't take yourself too seriously, have fun at work,
                                play hard, work hard and spread the love to your family members.</p>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section>

<section id="featured-services">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 box">
                <i class="ion-social-apple"></i>
                <h4 class="title"><a href="">iOS App Development</a></h4>

                <p class="description">Applications development (Native, Hybrid) for iPad, iPhone and iPod Touch, with
                    any level of complexity. </p>
            </div>

            <div class="col-lg-4 box box-bg">
                <i class="ion-social-android"></i>
                <h4 class="title"><a href="">Android App Development</a></h4>

                <p class="description">Applications development (Native, Hybrid) for Android phones and tablets, with
                    any level of complexity.</p>
            </div>

            <div class="col-lg-4 box">
                <i class="ion-laptop"></i>
                <h4 class="title"><a href="">Web Application</a></h4>

                <p class="description">Development of custom SaaS apps, web portals and websites. Development of web
                    services in support of online mobile apps.</p>
            </div>

        </div>
    </div>
</section>

<section id="about">
    <div class="container">

        <header class="section-header">
            <h3>About Us</h3>

            <p>We are an award winning, BD based, Development and Growth Agency. We build outstanding SaaS and Mobile
                experiences for high growth startups and scale businesses.

                We provide a full stack service, from concept validation, planning and design, through to development,
                launch and marketing strategies. All perfectly monitored and delivered with our dedicated project
                management team.</p>
        </header>

        <div class="row about-cols">

            <div class="col-md-4 wow fadeInUp">
                <div class="about-col">
                    <div class="img">
                        <img src="<?php echo $this->Html->url('/img/about/about-mission.jpg') ?>" alt=""
                             class="img-fluid">

                        <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Our Mission</a></h2>

                    <p>
                        Over the last 6 years, we've helped launch multi-million-pound SaaS products and design and
                        develop bespoke app projects, which gives us specialist knowledge and insights, that combined
                        makes us unique in the software development market.
                    </p>
                </div>
            </div>

            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                <div class="about-col">
                    <div class="img">
                        <img src="<?php echo $this->Html->url('/img/about/about-plan.jpg') ?>" alt="" class="img-fluid">

                        <div class="icon"><i class="ion-ios-list-outline"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Our Plan</a></h2>

                    <p class="text-justify" class="text-justify">
                        Our concept here at BusinessApps is to provide the convenience of in-house software development
                        and management while remaining cost and time-efficient. Our goal is always to leverage the power
                        of technology, to add value to your business.
                    </p>
                </div>
            </div>

            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                <div class="about-col">
                    <div class="img">
                        <img src="<?php echo $this->Html->url('/img/about/about-vision.jpg') ?>" alt=""
                             class="img-fluid">

                        <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Our Vision</a></h2>

                    <p class="text-justify">
                        We're not happy with OK. We always aim to deliver WOW to our customers, whether that be with our
                        value for money, intuitive software, or with great customer service. To innovate we need to be a
                        little crazy, think out of the box and be agile.

                    </p>
                </div>
            </div>

        </div>

    </div>
</section>


<section id="services">
    <div class="container">

        <header class="section-header wow fadeInUp">
            <h3>Services</h3>

            <p>We provide a full stack service, from concept validation, planning and design, through to development,
                launch and marketing strategies. All perfectly monitored and delivered with our dedicated project
                management team.</p>
        </header>

        <div class="row">
            <div class="col-lg-6 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                <div class="icon"><i class="ion-android-apps"></i></div>
                <h4 class="title"><a href=""><?php echo $services[2]['Service']['name']; ?></a></h4>

                <p class="description"><?php echo $services[2]['Service']['description']; ?></p>
            </div>
            <div class="col-lg-6 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                <div class="icon"><i class="ion-social-chrome"></i></div>
                <h4 class="title"><a href=""><?php echo $services[1]['Service']['name']; ?></a></h4>

                <p class="description"><?php echo $services[1]['Service']['description']; ?></p>
            </div>
            <div class="col-lg-6 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                <div class="icon"><i class="ion-pie-graph"></i></div>
                <h4 class="title"><a href=""><?php echo $services[0]['Service']['name']; ?></a></h4>

                <p class="description"><?php echo $services[0]['Service']['description']; ?></p>
            </div>

            <div class="col-lg-6 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                <div class="icon"><i class="ion-android-settings"></i></div>
                <h4 class="title"><a href=""><?php echo $services[3]['Service']['name']; ?></a></h4>

                <p class="description"><?php echo $services[3]['Service']['description']; ?></p>
            </div>


        </div>

    </div>
</section>

<section id="call-to-action" class="wow fadeIn">
    <div class="container text-center">
        <h3>Call To Action</h3>

        <p> Financial management,Marketing, sales and customer service,Communication and negotiation,Leadership,Project
            management and planning,
            Delegation and time management,Problem solving,Networking
            .</p>
        <a class="cta-btn" href="#">Call To Action</a>
    </div>
</section><!-- #call-to-action -->

<!--==========================
  Skills Section
============================-->
<section id="skills">
    <div class="container">

        <header class="section-header">
            <h3>Our Skills</h3>

            <p>Our Skills Program aims to assist individuals to gain the higher level skills required to secure
                employment or
                career advancement in a priority industry, or to transition to university.</p>
        </header>

        <div class="skills-content row">
            <div class="col-lg-6 col-md-6">
                <div class="skills-content">

                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">Objective C <i class="val">100%</i></span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0"
                             aria-valuemax="90">
                            <span class="skill">Swift <i class="val">90%</i></span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="95" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">Titanium <i class="val">95%</i></span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">Android <i class="val">100%</i></span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">Java <i class="val">100%</i></span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="skills-content">

                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">HTML, HTML5 <i class="val">100%</i></span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">CSS, CSS3 <i class="val">100%</i></span>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="97" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">JavaScript, jQuery, Angular.JS,React.JS <i class="val">95%</i></span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">Database (MySQL, PgSQL, MongoDB)<i class="val">100%</i></span>
                        </div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                             aria-valuemax="100">
                            <span class="skill">PHP(Laravel, Cake PHP, CI, Yii) <i class="val">100%</i></span>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>
</section>

<section id="facts" class="wow fadeIn">
    <div class="container">

        <header class="section-header">
            <h3>Facts</h3>

            <p>However, to explain to you how all this mistaken idea of denouncing pleasure and pain</p>
        </header>

        <div class="row counters">

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">274</span>

                <p>Clients</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">421</span>

                <p>Projects</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">1,364</span>

                <p>Hours Of Support</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up">18</span>

                <p>Hard Workers</p>
            </div>

        </div>

        <div class="facts-img">
            <img src="<?php echo $this->Html->url('/img/facts-img.png') ?>" alt="" class="img-fluid">
        </div>

    </div>
</section><!-- #facts -->

<!--==========================
  Portfolio Section
============================-->
<section id="portfolio" class="section-bg">
    <div class="container">

        <header class="section-header">
            <h3 class="section-title">Our Portfolio</h3>
        </header>

        <div class="row">
            <div class="col-lg-12">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    <li data-filter=".filter-iso">Android</li>
                    <li data-filter=".filter-android">ISO</li>
                    <li data-filter=".filter-web">Web</li>
                </ul>
            </div>
        </div>

        <div class="row portfolio-container">

            <div class="col-lg-4 col-md-6 portfolio-item filter-iso wow fadeInUp">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/letstalkapp.jpg') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/letstalkapp.jpg') ?>" data-lightbox="portfolio"
                           data-title="Let's Talk" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Let's Talk</a></h4>

                        <p>Android</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/admissionninja.png') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/admissionninja.png') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Admission Ninja" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Admission Ninja</a></h4>

                        <p>Web</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-iso wow fadeInUp" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/tradecarlinkapp.jpg') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/tradecarlinkapp.jpg') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Trade Car Link" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Trade Car Link</a></h4>

                        <p>Android</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-android wow fadeInUp">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/cabbieappiso.png') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/cabbieappiso.png') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Cabbie App UK" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Cabbie App UK</a></h4>

                        <p>ISO</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/broomly.png') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/broomly.png') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Broomly" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Broomly</a></h4>

                        <p>Web</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-iso wow fadeInUp" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/ulibralyandroid.jpg') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/ulibralyandroid.jpg') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="U-Library" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">U-Library</a></h4>

                        <p>Android</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-android wow fadeInUp">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/dogmateiso.png') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/dogmateiso.png') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Dog Mate" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Dog Mate</a></h4>

                        <p>ISO</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-android wow fadeInUp" data-wow-delay="0.1s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/obelishtousr.jpg') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/obelishtousr.jpg') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Obelish Tour" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Obelish Tour</a></h4>

                        <p>ISO</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                <div class="portfolio-wrap">
                    <figure>
                        <img src="<?php echo $this->Html->url('/img/portfolio/threebhai.png') ?>" class="portfolio-img" alt="">
                        <a href="<?php echo $this->Html->url('/img/portfolio/threebhai.png') ?>" class="link-preview"
                           data-lightbox="portfolio" data-title="Three Bhai" title="Preview"><i class="ion ion-eye"></i></a>
                        <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                    </figure>

                    <div class="portfolio-info">
                        <h4><a href="#">Three Bhai</a></h4>

                        <p>Web</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<section id="clients" class="wow fadeInUp">
    <div class="container">

        <header class="section-header">
            <h3>Our Clients</h3>
        </header>

        <div class="owl-carousel clients-carousel">
            <img src="<?php echo $this->Html->url('/img/clients/client-1.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-2.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-3.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-4.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-5.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-6.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-7.png') ?>" alt="">
            <img src="<?php echo $this->Html->url('/img/clients/client-8.png') ?>" alt="">
        </div>

    </div>
</section><!-- #clients -->

<section id="testimonials" class="section-bg wow fadeInUp">
    <div class="container">

        <header class="section-header">
            <h3>Testimonials</h3>
        </header>

        <div class="owl-carousel testimonials-carousel">

            <?php foreach($testimonials as $testimonial) {?>
                <div class="testimonial-item">
                    
                    <img src="<?php echo $this->Html->url('/files/client/'.$testimonial['Testimonial']['image_link'])?>" class="testimonial-img" alt="">
                    <h3><?php echo $testimonial['Testimonial']['name'] ?></h3>

                    <p>
                        <img src="<?php echo $this->Html->url('/img/quote-sign-left.png')?>" class="quote-sign-left" alt="">
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <?php echo $testimonial['Testimonial']['comment'] ?>
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>
            <?php } ?>

        </div>

    </div>
</section>
<section id="team">
    <div class="container">
        <div class="section-header wow fadeInUp">
            <h3>Meet The Team</h3>
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-6 wow fadeInUp">
                <div class="member">
                    <img src="<?php echo $this->Html->url('/img/team/team-1.jpg') ?>" class="img-fluid" alt="">

                    <div class="member-info">
                        <div class="member-info-content">
                            <h4>Raz Ahmed</h4>
                            <span>Founder & CEO</span>

                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="member">
                    <img src="<?php echo $this->Html->url('/img/team/team-2.jpg') ?>" class="img-fluid" alt="">

                    <div class="member-info">
                        <div class="member-info-content">
                            <h4>Chayan Mistry</h4>
                            <span>Sr. Android Developer</span>

                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                <div class="member">
                    <img src="<?php echo $this->Html->url('/img/team/team-3.jpg') ?>" class="img-fluid" alt="">

                    <div class="member-info">
                        <div class="member-info-content">
                            <h4>Mukul Hossen</h4>
                            <span>Web Developer</span>

                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="member">
                    <img src="<?php echo $this->Html->url('/img/team/team-4.jpg') ?>" class="img-fluid" alt="">

                    <div class="member-info">
                        <div class="member-info-content">
                            <h4>
                                Raqeeb Ahmed</h4>
                            <span>Android Developer</span>

                            <div class="social">
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-google-plus"></i></a>
                                <a href=""><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section><!-- #team -->

<!--==========================
  Contact Section
============================-->
<!--==========================
      Contact Section
    ============================-->
<section id="contact" class="section-bg wow fadeInUp">
    <div class="container">

        <div class="section-header">
            <h3>Contact Us</h3>

            <p>We would love to be your partner in realising your mobile app goals, please complete the form, call, email or pop in the office to talk further on how we can help.</p>
        </div>

        <div class="row contact-info">

            <div class="col-md-4">
                <div class="contact-address">
                    <i class="ion-ios-location-outline"></i>

                    <h3>Address</h3>
                    <address>Road#3, House # 384, Sonadanga R/A (2nd Phase) Khulna - 9100, Bangladesh</address>
                </div>
            </div>

            <div class="col-md-4">
                <div class="contact-phone">
                    <i class="ion-ios-telephone-outline"></i>

                    <h3>Phone Number</h3>

                    <p><a href="#">+8801724-160299</a></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="contact-email">
                    <i class="ion-ios-email-outline"></i>

                    <h3>Email</h3>

                    <p><a href="#">xor.solution@gmail.com</a></p>
                </div>
            </div>

        </div>

        <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form action="" method="post" role="form" class="contactForm">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                               data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>

                        <div class="validation"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email"
                               data-rule="email" data-msg="Please enter a valid email"/>

                        <div class="validation"></div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"
                           data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"/>

                    <div class="validation"></div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" data-rule="required"
                              data-msg="Please write something for us" placeholder="Message"></textarea>

                    <div class="validation"></div>
                </div>
                <div class="text-center">
                    <button type="submit">Send Message</button>
                </div>
            </form>
        </div>

    </div>
</section><!-- #contact -->

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-info">
                    <a href="#intro"><img src="img/logo.png" width="250px" alt="logo" title=""/></a>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">About us</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Services</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Contact Us</h4>

                    <p>
                        <?php echo $contact_information['ContactInformation']['first_address'] . ', </br>' . $contact_information['ContactInformation']['second_address'] ?>
                        <br>
                        <?php echo $contact_information['ContactInformation']['town'] . ', ' . $contact_information['ContactInformation']['country'] ?>
                        <br>
                        <strong>Phone:</strong> <a
                            href="<?php echo $contact_information['ContactInformation']['phone_1']; ?>"><?php echo $contact_information['ContactInformation']['phone_1']; ?></a>
                        <br>
                        <strong>Email:</strong> <a
                            href="mailto:<?php echo $contact_information['ContactInformation']['mail_1']; ?>"><?php echo $contact_information['ContactInformation']['mail_1']; ?></a><br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.facebook.com/xorsolution/" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>

                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Our Newsletter</h4>

                    <p>We're not just technologists, we're entrepreneurs at heart, with experience of growing ideas into
                        multi-million-pound businesses. Our value-add is helping you navigate funding, idea validation
                        and business planning, to scaling out and growing your business, underpinned by awesome
                        technology. </p>

                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; <?php echo date('Y') ?> XOR Software Solution.
        </div>
        <div class="credits">
            All rights reserved.
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
