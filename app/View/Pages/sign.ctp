<!DOCTYPE html>
<html lang="en">
<head>
	<title>form ragistration</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<style type="text/css">
	body {
    background:url(img/app_background.png) no-repeat;
    background-size: 100%;
}



.row h1 {
        font-size: 36px;
    background: #ffecb3;
    padding: 0px;
    margin: 0px;
    height: 62px;
    text-align: center;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding-top: 10px;
}
@media (min-width: 768px)
.container {
    width: 80%;
}
#contact-form  {
        width: 80%;
    margin: auto;
    margin-top: 20px;
}
.row {
    
    background-color:#ffffe6;
    margin: auto;
    margin-top: 450px;
    width: 82%;
    border-radius: 5px;
    height: 750px;
}
.btn-info   {
    width: 100%;
}

.form-box h1 span {
    font-weight: lighter;
}
.form-control   {
    height: 50px;
}
.alert {
     padding: 0px; 
     margin-bottom: 0px; 
    border: 1px solid transparent;
    border-radius: 4px;
}
.form-group {
    margin-bottom: 15px;
    height: 59px;
}
  
	</style>

</head>
<body>
	<div class="container">
	<div class="row">
		
        <h1>SIGN <span>UP</span></h1>
           
    	    <form role="form" id="contact-form">
            <!--User name field -->
                <div class="form-group">
                <div id="nameError" class="alert" role="alert"></div>
                <label for="form-name-field" class="sr-only">Name</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" id="form-name-field" value="" placeholder="Full Name">
                    </div>
                </div>
            
            <!--name field-->
            <div class="form-group">
                <div id="nameError" class="alert" role="alert"></div>
                <label for="form-name-field" class="sr-only">Name</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" id="form-name-field" value="" placeholder="User Name">
                    </div>
                </div>
            <!-- email field -->
                <div class="form-group">
                <div id="emailError" class="alert" role="alert"></div>
                <label for="form-email-field" class="sr-only">Email</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                        <input type="email" class="form-control" id="form-email-field" value="" placeholder="Email">
                    </div>
                </div>
            <!--college field-->
            <div class="form-group">
                <div id="collegeError" class="alert" role="alert"></div>
                <label for="form-phone-field" class="sr-only">Phone</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-book"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="College">
                    </div>
                </div>
            <!--address-1 field-->
            <div class="form-group">
                <div id="collegeError" class="alert" role="alert"></div>
                <label for="form-phone-field" class="sr-only">Phone</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="Address">
                    </div>
                </div>
             <!--City field-->
            <div class="form-group">
                <div id="collegeError" class="alert" role="alert"></div>
                <label for="form-phone-field" class="sr-only">Phone</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="City">
                    </div>
                </div>
            
           
            <!-- phone field -->
                <div class="form-group">
                <div id="phoneError" class="alert" role="alert"></div>
                <label for="form-phone-field" class="sr-only">Phone</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                        <input type="phone" class="form-control" id="form-phone-field" value="" placeholder="Mobile">
                    </div>
                </div>
            
            
            <!--password field-->
             <div class="form-group">
                <div id="emailError" class="alert" role="alert"></div>
                <label for="login-passwor" class="sr-only">Email</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                        <input type="password" class="form-control" id="login-passwor" value="" placeholder="password">
                    </div>
                </div>
            
            
            <button type="button" class="btn btn-info">SIGN IN</button>
    	    </form>
             
		</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	document.getElementById("contact-form").onsubmit = function() {
    var nameValidate = /[a-z]/;
    //Validate Name
    if(document.getElementById("form-name-field").value == "") {
        document.getElementById("nameError").innerHTML = "Please Enter A Name";
        document.getElementById("nameError").className = "alert-danger";
    }
}
</script>