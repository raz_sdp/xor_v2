<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="socialLinks form">
<?php echo $this->Form->create('SocialLink', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Social Link'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('icon', array('type'=>'file'));
		echo $this->Form->input('link');
		echo $this->Form->input('active');
		echo $this->Form->input('order');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Social Links'), array('action' => 'index')); ?></li>
	</ul>
</div>
