<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="socialLinks view">
<h2><?php echo __('Social Link'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Icon'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Link'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($socialLink['SocialLink']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Social Link'), array('action' => 'edit', $socialLink['SocialLink']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Social Link'), array('action' => 'delete', $socialLink['SocialLink']['id']), array(), __('Are you sure you want to delete # %s?', $socialLink['SocialLink']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Social Links'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Social Link'), array('action' => 'add')); ?> </li>
	</ul>
</div>
