<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="socialLinks form">
<?php echo $this->Form->create('SocialLink', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Social Link'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
        if(!empty($this->Form->data['SocialLink']['icon'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/logos/' . $this->request->data['SocialLink']['icon'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
		echo $this->Form->input('icon', array('type'=>'file'));
		echo $this->Form->input('link');
		echo $this->Form->input('active');
		echo $this->Form->input('order');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SocialLink.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SocialLink.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Social Links'), array('action' => 'index')); ?></li>
	</ul>
</div>
