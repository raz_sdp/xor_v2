<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="subMenus index">
	<h2><?php echo __('Sub Menus of menu: '); echo $menu_id;?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('link'); ?></th>
			<th><?php echo $this->Paginator->sort('order'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($subMenus as $subMenu): ?>
	<tr>
		<td><?php echo h($subMenu['name']); ?>&nbsp;</td>
		<td><?php echo h($subMenu['link']); ?>&nbsp;</td>
		<td><?php echo h($subMenu['order']); ?>&nbsp;</td>
		<td><?php echo h($subMenu['active']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'sub_menu_edit', $subMenu['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'sub_menu_delete', $subMenu['id']), array('confirm' => __('Are you sure you want to delete # %s?', $subMenu['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sub Menu'), array('action' => 'sub_menu_add', $menu_id)); ?></li>
	</ul>
</div>
