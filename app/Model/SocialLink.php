<?php
App::uses('AppModel', 'Model');

class SocialLink extends AppModel {

	public $displayField = 'name';

}
