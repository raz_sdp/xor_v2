<?php
App::uses('AppModel', 'Model');

class ProjectType extends AppModel {

	public $displayField = 'name';

}
