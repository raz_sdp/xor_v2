<?php
App::uses('AppModel', 'Model');

class SliderLayer extends AppModel {
    public $belongsTo = array(
        'SliderImage' => array(
            'className' => 'SliderImage',
            'foreignKey' => 'slider_image_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
