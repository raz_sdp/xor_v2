<?php
App::uses('AppController', 'Controller');

class PartnersController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Partner->recursive = 0;
		$this->set('partners', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Partner->exists($id)) {
			throw new NotFoundException(__('Invalid partner'));
		}
		$options = array('conditions' => array('Partner.' . $this->Partner->primaryKey => $id));
		$this->set('partner', $this->Partner->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['Partner']['logo'])) {
                $file_name = $this->_upload($this->request->data['Partner']['logo'], 'partners');
                $this->request->data['Partner']['logo'] = $file_name;
            } else {
                unset($this->request->data['Partner']['logo']);
            }
			$this->Partner->create();
			if ($this->Partner->save($this->request->data)) {
				$this->Session->setFlash(__('The partner has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The partner could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Partner->exists($id)) {
			throw new NotFoundException(__('Invalid partner'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Partner']['logo'])) {
                if(!empty($this->request->data['Partner']['logo']['name'])){
                    $file_name = $this->_upload($this->request->data['Partner']['logo'], 'client');
                    $this->request->data['Partner']['logo'] = $file_name;
                } else {
                    $options = array('conditions' => array('Partner.' . $this->Partner->primaryKey => $id));
                    $data = $this->Partner->find('first', $options);
                    $this->request->data['Partner']['logo'] = $data['Partner']['logo'];
                }
            } else {
                unset($this->request->data['Partner']['logo']);
            }
            $this->Partner->id = $id;
			if ($this->Partner->save($this->request->data)) {
				$this->Session->setFlash(__('The partner has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The partner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Partner.' . $this->Partner->primaryKey => $id));
			$this->request->data = $this->Partner->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->Partner->id = $id;
		if (!$this->Partner->exists()) {
			throw new NotFoundException(__('Invalid partner'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Partner->delete()) {
			$this->Session->setFlash(__('The partner has been deleted.'));
		} else {
			$this->Session->setFlash(__('The partner could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
