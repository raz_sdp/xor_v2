<?php
App::uses('AppController', 'Controller');

class SliderLayersController extends AppController {

	public $components = array('Paginator', 'Session');

    /*
        public function admin_index() {
            $this->SliderLayer->recursive = 0;
            $this->set('sliderLayers', $this->Paginator->paginate());
        }

        public function admin_view($id = null) {
            if (!$this->SliderLayer->exists($id)) {
                throw new NotFoundException(__('Invalid slider layer'));
            }
            $options = array('conditions' => array('SliderLayer.' . $this->SliderLayer->primaryKey => $id));
            $this->set('sliderLayer', $this->SliderLayer->find('first', $options));
        }

        public function admin_add() {
            if ($this->request->is('post')) {
                $this->SliderLayer->create();
                if ($this->SliderLayer->save($this->request->data)) {
                    $this->Session->setFlash(__('The slider layer has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The slider layer could not be saved. Please, try again.'));
                }
            }
        }

        public function admin_edit($id = null) {
            if (!$this->SliderLayer->exists($id)) {
                throw new NotFoundException(__('Invalid slider layer'));
            }
            if ($this->request->is(array('post', 'put'))) {
                if ($this->SliderLayer->save($this->request->data)) {
                    $this->Session->setFlash(__('The slider layer has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The slider layer could not be saved. Please, try again.'));
                }
            } else {
                $options = array('conditions' => array('SliderLayer.' . $this->SliderLayer->primaryKey => $id));
                $this->request->data = $this->SliderLayer->find('first', $options);
            }
        }

        public function admin_delete($id = null) {
            $this->SliderLayer->id = $id;
            if (!$this->SliderLayer->exists()) {
                throw new NotFoundException(__('Invalid slider layer'));
            }
            $this->request->allowMethod('post', 'delete');
            if ($this->SliderLayer->delete()) {
                $this->Session->setFlash(__('The slider layer has been deleted.'));
            } else {
                $this->Session->setFlash(__('The slider layer could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    */

    public function layers_of($image_id) {

    }

}
