<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
        //AuthComponent::_setTrace($this->Auth->user());
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if(!empty($this->request->data['User']['simple_pwd'])){
                $this->request->data['User']['simple_pwd'] = $this->request->data['User']['simple_pwd'];
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['simple_pwd']);
            }
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_login(){
        if ($this->request->is('post')) {

            $password = AuthComponent::password($this->request->data['User']['password']);
            #AuthComponent::_setTrace($password);
            $query = array(
                'conditions' => array(
                    'User.username' => $this->request->data['User']['username'],
                    'User.password' => $password,
                ),
            );
            $is_exist = $this->User->find('first', $query);
            if(!empty($is_exist)){
                $this->Auth->login($this->request->data['User']);
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Auth->logout();
                $this->Session->setFlash(__('Username or password is incorrect'));
            }
        }
    }

    public function admin_logout()
    {
        $this->Auth->logout();
        return $this->redirect($this->Auth->logout());
    }
    public function send_email(){
        //pr($this->request->data);die;
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        $from = $this->request->data['email'];
        $subject = $this->request->data['subject'];
        $message = $this->request->data['message'];
        $name = $this->request->data['name'];
        try {
            App::uses('CakeEmail', 'Network/Email');
            $email = new CakeEmail();
            $email->config('smtp');
            $email->subject($subject);
            $email->from($from);
            $email->to('xor.solution@gmail.com');
            $email->send($message."<br><br><br> Regards <br><b>".$name."</b>");

            /*$this->_send_email(
                $email,
                "Welcome to Islamic App!",
                "Dear " . $is_user['User']['fullname'] . ",<br/>Your account  password has been reset. Your new password is <b>".$new_passkey."<br/><br/>Regards,<br/><br/>The Islamic App Team",
                'welcome',
                array('title' => 'Reseted your password')
            );*/
            die(json_encode(array('success' => true)));
        } catch (Exception $e) {
            //echo($e);
            die(json_encode(array('success' => false)));
        }
    }
}
