<?php
App::uses('AppController', 'Controller');

class SocialLinksController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->SocialLink->recursive = 0;
		$this->set('socialLinks', $this->Paginator->paginate());
	}

    // no need to view
	/*
	public function admin_view($id = null) {
		if (!$this->SocialLink->exists($id)) {
			throw new NotFoundException(__('Invalid social link'));
		}
		$options = array('conditions' => array('SocialLink.' . $this->SocialLink->primaryKey => $id));
		$this->set('socialLink', $this->SocialLink->find('first', $options));
	}
	*/

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['SocialLink']['icon']['name'])) {
                $file_name = $this->_upload($this->request->data['SocialLink']['icon'], 'logos');
                $this->request->data['SocialLink']['icon'] = $file_name;
            } else {
                unset($this->request->data['SocialLink']['icon']);
            }
			$this->SocialLink->create();
			if ($this->SocialLink->save($this->request->data)) {
				$this->Session->setFlash(__('The social link has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social link could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->SocialLink->exists($id)) {
			throw new NotFoundException(__('Invalid social link'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['SocialLink']['icon']['name'])) {
                $file_name = $this->_upload($this->request->data['SocialLink']['icon'], 'logos');
                $this->request->data['SocialLink']['icon'] = $file_name;
            } else {
                unset($this->request->data['SocialLink']['icon']);
            }
			if ($this->SocialLink->save($this->request->data)) {
				$this->Session->setFlash(__('The social link has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social link could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SocialLink.' . $this->SocialLink->primaryKey => $id));
			$this->request->data = $this->SocialLink->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->SocialLink->id = $id;
		if (!$this->SocialLink->exists()) {
			throw new NotFoundException(__('Invalid social link'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SocialLink->delete()) {
			$this->Session->setFlash(__('The social link has been deleted.'));
		} else {
			$this->Session->setFlash(__('The social link could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
