<?php
App::uses('AppController', 'Controller');

class TestimonialsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Testimonial->recursive = 0;
		$this->set('testimonials', $this->Paginator->paginate());
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['Testimonial']['image_link'])) {
                $file_name = $this->_upload($this->request->data['Testimonial']['image_link'], 'client');
                $this->request->data['Testimonial']['image_link'] = $file_name;
            } else {
                unset($this->request->data['Testimonial']['image_link']);
            }
			$this->Testimonial->create();
			if ($this->Testimonial->save($this->request->data)) {
				$this->Session->setFlash(__('The testimonial has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Testimonial->exists($id)) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Testimonial']['image_link'])) {
                if(!empty($this->request->data['Testimonial']['image_link']['name'])){
                    $file_name = $this->_upload($this->request->data['Testimonial']['image_link'], 'client');
                    $this->request->data['Testimonial']['image_link'] = $file_name;
                } else {
                    $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
                    $data = $this->Testimonial->find('first', $options);
                    $this->request->data['Testimonial']['image_link'] = $data['Testimonial']['image_link'];
                }
            } else {
                unset($this->request->data['Testimonial']['image_link']);
            }
            $this->Testimonial->id = $id;
			if ($this->Testimonial->save($this->request->data)) {
				$this->Session->setFlash(__('The testimonial has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
			$this->request->data = $this->Testimonial->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Testimonial->delete()) {
			$this->Session->setFlash(__('The testimonial has been deleted.'));
		} else {
			$this->Session->setFlash(__('The testimonial could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
