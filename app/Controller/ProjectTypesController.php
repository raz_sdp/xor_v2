<?php
App::uses('AppController', 'Controller');
/**
 * ProjectTypes Controller
 *
 * @property ProjectType $ProjectType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProjectType->recursive = 0;
		$this->set('projectTypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProjectType->exists($id)) {
			throw new NotFoundException(__('Invalid project type'));
		}
		$options = array('conditions' => array('ProjectType.' . $this->ProjectType->primaryKey => $id));
		$this->set('projectType', $this->ProjectType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProjectType->create();
			if ($this->ProjectType->save($this->request->data)) {
				$this->Session->setFlash(__('The project type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProjectType->exists($id)) {
			throw new NotFoundException(__('Invalid project type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectType->save($this->request->data)) {
				$this->Session->setFlash(__('The project type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectType.' . $this->ProjectType->primaryKey => $id));
			$this->request->data = $this->ProjectType->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProjectType->id = $id;
		if (!$this->ProjectType->exists()) {
			throw new NotFoundException(__('Invalid project type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectType->delete()) {
			$this->Session->setFlash(__('The project type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
