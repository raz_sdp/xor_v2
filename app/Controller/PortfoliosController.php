<?php
App::uses('AppController', 'Controller');

class PortfoliosController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Portfolio->recursive = 0;
		$this->set('portfolios', $this->Paginator->paginate());
	}

    /*public function admin_view($id = null) {
        if (!$this->Portfolio->exists($id)) {
            throw new NotFoundException(__('Invalid portfolio'));
        }
        $options = array('conditions' => array('Portfolio.' . $this->Portfolio->primaryKey => $id));
        $this->set('portfolio', $this->Portfolio->find('first', $options));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Portfolio->create();
            if ($this->Portfolio->save($this->request->data)) {
                $this->Session->setFlash(__('The portfolio has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The portfolio could not be saved. Please, try again.'));
            }
        }
        $projects = $this->Portfolio->Project->find('list');
        $this->set(compact('projects'));
    }*/

	public function admin_edit($id = null) {
		if (!$this->Portfolio->exists($id)) {
			throw new NotFoundException(__('Invalid portfolio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Portfolio->save($this->request->data)) {
				$this->Session->setFlash(__('The portfolio has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The portfolio could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Portfolio.' . $this->Portfolio->primaryKey => $id));
			$this->request->data = $this->Portfolio->find('first', $options);
		}
		$projects = $this->Portfolio->Project->find('list');
		$this->set(compact('projects'));
	}

	public function admin_delete($id = null) {
		$this->Portfolio->id = $id;
		if (!$this->Portfolio->exists()) {
			throw new NotFoundException(__('Invalid portfolio'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Portfolio->delete()) {
			$this->Session->setFlash(__('The portfolio has been deleted.'));
		} else {
			$this->Session->setFlash(__('The portfolio could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
