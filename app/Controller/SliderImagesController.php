<?php
App::uses('AppController', 'Controller');

class SliderImagesController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
        $this->SliderImage->recursive = 1;
        $sliderImages = $this->Paginator->paginate();
        #AuthComponent::_setTrace($sliderImages);
        $this->set(compact('sliderImages'));
	}

	/*public function admin_view($id = null) {
		if (!$this->SliderImage->exists($id)) {
			throw new NotFoundException(__('Invalid slider image'));
		}
		$options = array('conditions' => array('SliderImage.' . $this->SliderImage->primaryKey => $id));
		$this->set('sliderImage', $this->SliderImage->find('first', $options));
	}*/

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['SliderImage']['image_link'])) {
                $file_name = $this->_upload($this->request->data['SliderImage']['image_link'], 'sliders');
                $this->request->data['SliderImage']['image_link'] = $this->slider_img_dir() . $file_name;
            } else {
                unset($this->request->data['SliderImage']['image_link']);
            }

            $this->SliderImage->create();
			if ($this->SliderImage->save($this->request->data)) {
				$this->Session->setFlash(__('The slider image has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slider image could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->SliderImage->exists($id)) {
			throw new NotFoundException(__('Invalid slider image'));
		}
		if ($this->request->is(array('post', 'put'))) {
            #AuthComponent::_setTrace($this->request->data);
            if (!empty($this->request->data['SliderImage']['image_link'])) {
                if(!empty($this->request->data['SliderImage']['image_link']['name'])){
                    $file_name = $this->_upload($this->request->data['SliderImage']['image_link'], 'sliders');
                    $this->request->data['SliderImage']['image_link'] = $this->slider_img_dir() . $file_name;
                } else {
                    $options = array('conditions' => array('SliderImage.' . $this->SliderImage->primaryKey => $id));
                    $data = $this->SliderImage->find('first', $options);
                    $this->request->data['SliderImage']['image_link'] = $data['SliderImage']['image_link'];
                }
            } else {
                unset($this->request->data['SliderImage']['image_link']);
            }
            $this->SliderImage->id = $id;
			if ($this->SliderImage->save($this->request->data)) {
				$this->Session->setFlash(__('The slider image has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slider image could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SliderImage.' . $this->SliderImage->primaryKey => $id));
			$this->request->data = $this->SliderImage->find('first', $options);
		}
	}

    public function admin_default($id = null) {
        if (!$this->SliderImage->exists($id)) {
            throw new NotFoundException(__('Invalid slider image'));
        }

        $this->SliderImage->id = $id;
        $data = $this->SliderImage->findById($id);
        if (! $data['SliderImage']['is_default']) {
            $this->Session->setFlash(__('The slider image isn\'t default.'));
            return $this->redirect(array('action' => 'index'));
        }
        $data['SliderImage']['image_link'] = $data['SliderImage']['default_image'];
        $data['SliderImage']['order'] = $data['SliderImage']['default_order'];
        $data['SliderImage']['transition_type'] = $data['SliderImage']['default_transition'];
        $data['SliderImage']['speed'] = $data['SliderImage']['default_speed'];

        if ($this->SliderImage->save($data)) {
            $this->Session->setFlash(__('The slider image has been reverted to default.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('The slider image could not be saved. Please, try again.'));
        }
    }

	public function admin_delete($id = null) {
		$this->SliderImage->id = $id;
		if (!$this->SliderImage->exists()) {
			throw new NotFoundException(__('Invalid slider image'));
		}

        $data = $this->SliderImage->findById($id);
        if(!$data['SliderImage']['is_default']){
            $this->request->allowMethod('post', 'delete');
            if ($this->SliderImage->delete()) {
                $this->Session->setFlash(__('The slider image has been deleted.'));
            } else {
                $this->Session->setFlash(__('The slider image could not be deleted. Please, try again.'));
            }
        } else {
            $this->Session->setFlash(__('The default slider image could not be deleted.'));
        }
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_general_settings() {
        $this->loadModel('SliderGeneralSetting');
        $id = 1;
        if ($this->request->is(array('post', 'put'))) {
            $this->SliderGeneralSetting->id = $id;
            if ($this->SliderGeneralSetting->save($this->request->data)) {
                $this->Session->setFlash(__('Slider settings have been updated.'));
                return $this->redirect(array('action' => 'general_settings'));
            } else {
                $this->Session->setFlash(__('Slider settings could not be updated. Please, try again.'));
            }
        } else {
            $this->request->data  = $this->SliderGeneralSetting->findById($id);
        }
    }

    public function admin_default_general_setting(){
        $this->loadModel('SliderGeneralSetting');
        $id = 1;
        $data = $this->SliderGeneralSetting->findById($id);
        $data['SliderGeneralSetting']['delay'] = $data['SliderGeneralSetting']['default_delay'];
        $data['SliderGeneralSetting']['start_width'] = $data['SliderGeneralSetting']['default_start_width'];
        $data['SliderGeneralSetting']['start_height'] = $data['SliderGeneralSetting']['default_start_height'];
        $data['SliderGeneralSetting']['hide_thumbs'] = $data['SliderGeneralSetting']['default_hide_thumbs'];
        #$this->SliderGeneralSetting->id = $id;
        if ($this->SliderGeneralSetting->save($data)) {
            $this->Session->setFlash(__('Slider settings have been updated to default.'));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Slider settings could not be updated. Please, try again.'));
        }

    }

    public function admin_all_default(){
        echo "reset everything to default"; die();
    }

    public function admin_preview(){
        $this->SliderImage->recursive = 1;
        $data = $this->SliderImage->find('all');

        $this->loadModel('SliderGeneralSetting');
        $id = 1;
        $general = $this->SliderGeneralSetting->findById($id);
        $this->set(compact('data','general'));
    }

    public function admin_layers($id) {
        $this->SliderImage->recursive = 1;
        $data = $this->SliderImage->findById($id);
        $layers = $data['SliderLayer'];
        $image_id = $id;
        $this->set(compact('layers','image_id'));
    }

    public function admin_edit_layer($id) {
        $this->loadModel('SliderLayer');
        if (!$this->SliderLayer->exists($id)) {
            throw new NotFoundException(__('Invalid slider layer'));
        }
        if ($this->request->is(array('post', 'put'))) {
            #AuthComponent::_setTrace($this->request->data);
            if($this->request->data['SliderLayer']['content_type'] == 'img'){
                if (!empty($this->request->data['SliderLayer']['image'])) {
                    if(!empty($this->request->data['SliderLayer']['image']['name'])){
                        $file_name = $this->_upload($this->request->data['SliderLayer']['image'], 'sliders');
                        $this->request->data['SliderLayer']['image'] = $this->slider_img_dir() . $file_name;
                    } else {
                        $options = array('conditions' => array('SliderLayer.' . $this->SliderLayer->primaryKey => $id));
                        $data = $this->SliderLayer->find('first', $options);
                        $this->request->data['SliderLayer']['image'] = $data['SliderLayer']['image'];
                    }
                } else {
                    unset($this->request->data['SliderLayer']['image']);
                }
            }  else {
                unset($this->request->data['SliderLayer']['image']);
            }
            $this->SliderLayer->id = $id;
            $slider_id = $this->request->data['SliderLayer']['slider_image_id'];
            if ($this->SliderLayer->save($this->request->data)) {
                $this->Session->setFlash(__('The slider layer has been saved.'));
                return $this->redirect(array('action' => 'layers', $slider_id));
            } else {
                $this->Session->setFlash(__('The slider layer could not be saved. Please, try again.'));
            }
        } else {
            $this->SliderLayer->recursive = -1;
            $this->request->data = $this->SliderLayer->findById($id);
        }


    }

    public function admin_add_layer($id) {
        $this->loadModel('SliderLayer');
        if (!$this->SliderImage->exists($id)) {
            throw new NotFoundException(__('Invalid slider image'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['SliderLayer']['slider_image_id'] = $id;

            if($this->request->data['SliderLayer']['content_type'] == 'img'){
                if (!empty($this->request->data['SliderLayer']['image'])) {
                    if(!empty($this->request->data['SliderLayer']['image']['name'])){
                        $file_name = $this->_upload($this->request->data['SliderLayer']['image'], 'sliders');
                        $this->request->data['SliderLayer']['image'] = $this->slider_img_dir() . $file_name;
                    }
                } else {
                    unset($this->request->data['SliderLayer']['image']);
                }
            }  else {
                unset($this->request->data['SliderLayer']['image']);
            }

            if ($this->SliderLayer->save($this->request->data)) {
                $this->Session->setFlash(__('The slider layer has been saved.'));
                return $this->redirect(array('action' => 'layers', $id));
            } else {
                $this->Session->setFlash(__('The slider layer could not be saved. Please, try again.'));
            }
        } else {
            $image_id = $id;
            $this->set(compact('image_id'));
        }
    }

    public function admin_delete_layer($id){
        $this->loadModel('SliderLayer');
        $this->SliderLayer->id = $id;
        if (!$this->SliderLayer->exists()) {
            throw new NotFoundException(__('Invalid slider layer'));
        }
        $data = $this->SliderLayer->findById($id);
        $this->request->allowMethod('post', 'delete');
        if ($this->SliderLayer->delete()) {
            $this->Session->setFlash(__('The slider layer has been deleted.'));
        } else {
            $this->Session->setFlash(__('The slider layer could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'layers', $data['SliderLayer']['slider_image_id']));
    }

}
