<?php
App::uses('AppController', 'Controller');

class SubMenusController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->SubMenu->recursive = 0;
		$this->set('subMenus', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->SubMenu->exists($id)) {
			throw new NotFoundException(__('Invalid sub menu'));
		}
		$options = array('conditions' => array('SubMenu.' . $this->SubMenu->primaryKey => $id));
		$this->set('subMenu', $this->SubMenu->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SubMenu->create();
			if ($this->SubMenu->save($this->request->data)) {
				$this->Session->setFlash(__('The sub menu has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sub menu could not be saved. Please, try again.'));
			}
		}
		$menus = $this->SubMenu->Menu->find('list');
		$this->set(compact('menus'));
	}

	public function admin_edit($id = null) {
		if (!$this->SubMenu->exists($id)) {
			throw new NotFoundException(__('Invalid sub menu'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SubMenu->save($this->request->data)) {
				$this->Session->setFlash(__('The sub menu has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sub menu could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SubMenu.' . $this->SubMenu->primaryKey => $id));
			$this->request->data = $this->SubMenu->find('first', $options);
		}
		$menus = $this->SubMenu->Menu->find('list');
		$this->set(compact('menus'));
	}

	public function admin_delete($id = null) {
		$this->SubMenu->id = $id;
		if (!$this->SubMenu->exists()) {
			throw new NotFoundException(__('Invalid sub menu'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SubMenu->delete()) {
			$this->Session->setFlash(__('The sub menu has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sub menu could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
