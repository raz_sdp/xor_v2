<?php
App::uses('AppController', 'Controller');
/**
 * CustomOrders Controller
 *
 * @property CustomOrder $CustomOrder
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CustomOrdersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CustomOrder->recursive = 0;
		$this->set('customOrders', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CustomOrder->exists($id)) {
			throw new NotFoundException(__('Invalid custom order'));
		}
		$options = array('conditions' => array('CustomOrder.' . $this->CustomOrder->primaryKey => $id));
		$this->set('customOrder', $this->CustomOrder->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CustomOrder->create();
			if ($this->CustomOrder->save($this->request->data)) {
				$this->Session->setFlash(__('The custom order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom order could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CustomOrder->exists($id)) {
			throw new NotFoundException(__('Invalid custom order'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomOrder->save($this->request->data)) {
				$this->Session->setFlash(__('The custom order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom order could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CustomOrder.' . $this->CustomOrder->primaryKey => $id));
			$this->request->data = $this->CustomOrder->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CustomOrder->id = $id;
		if (!$this->CustomOrder->exists()) {
			throw new NotFoundException(__('Invalid custom order'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomOrder->delete()) {
			$this->Session->setFlash(__('The custom order has been deleted.'));
		} else {
			$this->Session->setFlash(__('The custom order could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
