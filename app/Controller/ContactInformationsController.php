<?php
App::uses('AppController', 'Controller');

class ContactInformationsController extends AppController {

	public $components = array('Paginator', 'Session');

    //
    // can't create new settings or delete it. edit only.
    //

	/*
	public function admin_index() {
		$this->ContactInformation->recursive = 0;
		$this->set('contactInformations', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->ContactInformation->exists($id)) {
			throw new NotFoundException(__('Invalid contact information'));
		}
		$options = array('conditions' => array('ContactInformation.' . $this->ContactInformation->primaryKey => $id));
		$this->set('contactInformation', $this->ContactInformation->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ContactInformation->create();
			if ($this->ContactInformation->save($this->request->data)) {
				$this->Session->setFlash(__('The contact information has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact information could not be saved. Please, try again.'));
			}
		}
	}

    public function admin_delete($id = null) {
        $this->ContactInformation->id = $id;
        if (!$this->ContactInformation->exists()) {
            throw new NotFoundException(__('Invalid contact information'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->ContactInformation->delete()) {
            $this->Session->setFlash(__('The contact information has been deleted.'));
        } else {
            $this->Session->setFlash(__('The contact information could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
	*/

	public function admin_edit($id = null) {
		if (!$this->ContactInformation->exists($id)) {
			throw new NotFoundException(__('Invalid contact information'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $add_1 = $this->request->data['ContactInformation']['first_address'];
            $add_2 = $this->request->data['ContactInformation']['second_address'];
            $town = $this->request->data['ContactInformation']['town'];
            $country = $this->request->data['ContactInformation']['country'];
            if(!empty($add_1) && !empty($add_2) && !empty($town) && !empty($country) ) {
                $address = $add_1 . ", " . $add_2 . ", " . $town . ", " . $country;
                $lat_lng_obj = $this->lat_lang($address);
                $lat_lng = $lat_lng_obj['results'][0]['geometry']['location'];
                $this->request->data['ContactInformation']['lat'] = $lat_lng['lat'];
                $this->request->data['ContactInformation']['lng'] = $lat_lng['lng'];
            }

			if ($this->ContactInformation->save($this->request->data)) {
				$this->Session->setFlash(__('The contact information has been saved.'));
				return $this->redirect(array('action' => 'edit', 1, 'admin'=>true));
			} else {
				$this->Session->setFlash(__('The contact information could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ContactInformation.' . $this->ContactInformation->primaryKey => $id));
			$this->request->data = $this->ContactInformation->find('first', $options);
		}
	}
}
