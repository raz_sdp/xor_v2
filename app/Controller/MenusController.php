<?php
App::uses('AppController', 'Controller');

class MenusController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Menu->recursive = 0;
		$this->set('menus', $this->Paginator->paginate());
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Menu->create();
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->request->data = $this->Menu->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(__('Invalid menu'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Menu->delete()) {
			$this->Session->setFlash(__('The menu has been deleted.'));
		} else {
			$this->Session->setFlash(__('The menu could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_sub_menus($menu_id) {
        $this->Menu->recursive = 1;
        $data = $this->Menu->findById($menu_id);
        $subMenus = $data['SubMenu'];
        $this->set(compact('subMenus','menu_id'));
    }

    public function admin_sub_menu_add($menu_id) {
        $this->loadModel('SubMenu');
        if ($this->request->is('post')) {
            $this->SubMenu->create();
            if ($this->SubMenu->save($this->request->data)) {
                $this->Session->setFlash(__('The SubMenu has been saved.'));
                return $this->redirect(array('action' => 'sub_menus', $menu_id));
            } else {
                $this->Session->setFlash(__('The SubMenu could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('menu_id'));
    }

    public function admin_sub_menu_edit($id) {
        $this->loadModel('SubMenu');
        if (!$this->SubMenu->exists($id)) {
            throw new NotFoundException(__('Invalid SubMenu'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $menu_id = $this->request->data['SubMenu']['menu_id'];
            $this->SubMenu->id = $id;
            if ($this->SubMenu->save($this->request->data)) {
                $this->Session->setFlash(__('The SubMenu has been saved.'));
                return $this->redirect(array('action' => 'sub_menus', $menu_id));
            } else {
                $this->Session->setFlash(__('The SubMenu could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SubMenu.' . $this->SubMenu->primaryKey => $id));
            $this->request->data = $this->SubMenu->find('first', $options);
        }
    }

    public function admin_sub_menu_delete($id) {
        $this->loadModel('SubMenu');
        $this->SubMenu->id = $id;
        if (!$this->SubMenu->exists()) {
            throw new NotFoundException(__('Invalid SubMenu'));
        }
        $data = $this->SubMenu->findById($id);
        $this->request->allowMethod('post', 'delete');
        if ($this->SubMenu->delete()) {
            $this->Session->setFlash(__('The SubMenu has been deleted.'));
        } else {
            $this->Session->setFlash(__('The SubMenu could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'sub_menus', $data['SubMenu']['menu_id']));
    }

}
