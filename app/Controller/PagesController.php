<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

	public $layout = 'public';
	public function display() {		
		$this->autolayout = false;
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}

        $this->loadModel("SliderImage");
        $this->SliderImage->recursive = 1;
        $data = $this->SliderImage->find('all');

        $this->loadModel('SliderGeneralSetting');
        $id = 1;
        $general = $this->SliderGeneralSetting->findById($id);

        $this->loadModel("Service");
        $this->Service->recursive = 0;
        $services = $this->Service->find('all');

        $this->loadModel("Portfolio");
        $this->Portfolio->recursive = 1;
        $portfolio = $this->Portfolio->find('all');

        $this->loadModel("TeamMember");
        $this->TeamMember->recursive = 1;
        $team = $this->TeamMember->find('all', array(
            'conditions' => array('TeamMember.active =' => '1')
        ));

        $this->loadModel("Testimonial");
        $this->Testimonial->recursive = 1;
        $testimonials = $this->Testimonial->find('all', array(
            'conditions' => array('Testimonial.active =' => '1')
        ));

        $this->loadModel("Partner");
        $this->Partner->recursive = 1;
        $partners = $this->Partner->find('all', array(
            'conditions' => array('Partner.active =' => '1'),
            'limit' => 8
        ));

        $this->loadModel("GeneralSetting");
        $this->GeneralSetting->recursive = 1;
        $general_setting = $this->GeneralSetting->find('first');
        /*Menues*/
        $this->loadModel("Menu");        
        $menues = $this->Menu->find('all',array(
            'conditions' => array('Menu.active =' => '1'),
            'order' => array('Menu.order'=>'ASC')
        ));
        #AuthComponent::_setTrace($menues);

        $this->loadModel("SocialLink");
        $this->SocialLink->recursive = 1;
        $social_links = $this->SocialLink->find('all', array(
            'conditions' => array('SocialLink.active =' => '1'),
            'order' => array('SocialLink.order')
        ));
        $this->loadModel("ContactInformation");
        $this->ContactInformation->recursive = 1;
        $contact_information = $this->ContactInformation->find('first');

        $this->set(compact('menues','general_setting','social_links','contact_information','data','general','services','portfolio','team','testimonials','partners'));
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
