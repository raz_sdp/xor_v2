/**
 * Created with JetBrains PhpStorm.
 * User: Raaz Ahmed
 * Date: 11/24/13
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    $("#mail_send").click(function(){
        var name=$("#author").val();
        var email=$("#email").val();
        var subject=$("#subject").val();
        var message=$("#message").val();
        var site_url="/myblog/index.php";
        $.ajax({
        Type:"POST",
        url:site_url+"/site/mail",
        data:{name:name,email:email,subject:subject,message:message},
            success:function(response){
                $("html,body").animate({scrollTop:'0px'},300);
                $(".display_ajax_message").html(response);
                $(".display_ajax_message").fadeIn(400);
            }
        });
        return false;
    });

    /*================Read More=====*/
    $(".readmore").click(function(){
       var id=$(this).attr('id');
        $("#heading_"+id).hide();
        $("#description_"+id).show();
    });
});