-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 23, 2018 at 01:59 AM
-- Server version: 5.5.59-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xorbdcom_xor`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `description` text,
  `features` text,
  `price` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_informations`
--

CREATE TABLE `contact_informations` (
  `id` int(11) NOT NULL,
  `first_address` varchar(255) DEFAULT NULL,
  `second_address` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `phone_1` varchar(255) DEFAULT NULL,
  `phone_2` varchar(255) DEFAULT NULL,
  `mail_1` varchar(255) DEFAULT NULL,
  `mail_2` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_informations`
--

INSERT INTO `contact_informations` (`id`, `first_address`, `second_address`, `town`, `country`, `lat`, `lng`, `phone_1`, `phone_2`, `mail_1`, `mail_2`, `created`, `modified`) VALUES
(1, 'Road#3, House # 384', 'Sonadanga R/A (2nd Phase)', 'Khulna - 9100', 'Bangladesh', '22.8190874', '89.545522', '+8801724-160299', '', 'xor.solution@gmail.com', 'raz.abcoder@gmail.com', '2015-10-30 10:25:07', '2018-01-20 09:54:31');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `custom_orders`
--

CREATE TABLE `custom_orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `project_type` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `requirements_link` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `about` text,
  `why_us` text,
  `copyright_text` varchar(255) DEFAULT NULL,
  `clients` int(11) DEFAULT NULL,
  `coffee` int(11) DEFAULT NULL,
  `projects` int(11) DEFAULT NULL,
  `work_hour` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `company_name`, `logo`, `slogan`, `about`, `why_us`, `copyright_text`, `clients`, `coffee`, `projects`, `work_hour`, `created`, `modified`) VALUES
(1, 'XOR Soft. Solution', '1516269370389.png', 'Way to go, out of the box', '', '', 'copyright', 20, 10650, 500, 16100, '2015-10-29 08:41:36', '2018-01-18 03:56:10');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `link`, `order`, `active`, `created`, `modified`) VALUES
(1, 'Home', 'http://www.xorbd.com/#home', 1, 1, '2015-11-20 07:29:53', '2016-03-24 07:18:42'),
(4, 'About', 'http://www.xorbd.com/#about', 3, 1, '2015-11-20 09:40:11', '2016-03-24 06:22:26'),
(5, 'Services', 'http://www.xorbd.com/#services', 2, 1, '2016-01-06 07:37:16', '2016-03-24 07:18:01'),
(6, 'Portfolio', 'http://www.xorbd.com/#portfolio', 4, 1, '2016-01-06 07:39:57', '2016-03-24 07:18:11'),
(7, 'Contact', 'http://www.xorbd.com/#contact', 6, 1, '2016-01-06 07:40:16', '2016-03-24 07:18:20'),
(8, 'Team', 'http://www.xorbd.com/#team', 5, 1, '2016-01-06 07:44:49', '2016-03-24 07:18:29');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `link`, `logo`, `active`, `created`, `modified`) VALUES
(1, 'AppInstitute', 'http://appinstitute.com/', '1456913609898.png', 1, '2016-01-21 09:28:37', '2016-03-02 11:13:29'),
(2, 'ZotSell.com', 'https://www.zotsell.com/', '1456913632384.png', 1, '2016-01-21 09:41:51', '2016-03-02 11:13:53'),
(5, 'Indus Net Technologies Pvt. Ltd', 'https://www.indusnet.co.in/', '1456914484319.png', 1, '2016-01-21 09:25:11', '2016-03-02 11:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `project_id`, `order`, `created`, `modified`) VALUES
(2, 1, 1, '2015-11-24 07:47:55', '2015-11-24 07:48:08'),
(3, 3, 2, '2015-11-24 12:07:51', '2015-11-24 12:08:00'),
(4, 5, NULL, '2016-03-02 08:24:55', '2016-03-02 08:24:55'),
(5, 7, NULL, '2016-03-02 09:39:04', '2016-03-02 09:39:04'),
(6, 6, NULL, '2016-03-02 09:40:03', '2016-03-02 09:40:03'),
(7, 8, NULL, '2016-03-02 09:40:09', '2016-03-02 09:40:09'),
(8, 9, NULL, '2016-03-02 10:37:39', '2016-03-02 10:37:39'),
(9, 10, NULL, '2016-03-02 10:45:49', '2016-03-02 10:45:49'),
(10, 11, NULL, '2016-03-02 10:57:00', '2016-03-02 10:57:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `video_link` varchar(255) DEFAULT NULL,
  `description` text,
  `features` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `short_desc` varchar(255) DEFAULT NULL,
  `description` text,
  `features` text,
  `image_link` varchar(255) DEFAULT NULL,
  `video_link` varchar(255) DEFAULT NULL,
  `live_demo_link` varchar(255) DEFAULT NULL,
  `google_link` varchar(255) DEFAULT NULL,
  `apple_link` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_organization` varchar(255) DEFAULT NULL,
  `client_testimonial` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `status`, `short_desc`, `description`, `features`, `image_link`, `video_link`, `live_demo_link`, `google_link`, `apple_link`, `client_name`, `client_organization`, `client_testimonial`, `created`, `modified`) VALUES
(1, 'ULibrary', '', '', 'iPhone and iPad App development, ULibrary provides library users wordlwide a way to checkout, download and consume eAudio and eBook versions of the libraries paper based books!\r\n\r\nAdditionally a practice mode will allow users to play the memory game, without purchase, which will display in App Ads.', '', '1456847593256.png', NULL, '', '', '', 'Justin Ahurst', 'AppInstitute', '', '2015-11-20 10:56:42', '2016-03-02 13:03:13'),
(3, 'TradeCarlink', '', '', '', '\r\n', '1456903460208.png', NULL, '', '', '', 'Justin Ahurst', 'AppInstitute', '', '2015-11-24 12:07:34', '2016-03-02 13:06:16'),
(5, 'Katchup', '', '', 'iPhone & iPad App development, KatchUp is a new way of sharing online with only the people you trust, not the whole world. Organise your photos and videos into moments on your own personal timeline.', '', '1456903482163.png', NULL, '', '', '', 'Justin Ahurst', 'AppInstitute', '', '2016-01-21 13:52:25', '2016-03-02 13:10:52'),
(6, 'LetsTalk', '', '', 'iPhone App development, LetsTalk is the simple messaging app that lets you talk to friends in style and earn money for talking!', '', '1456907074568.png', NULL, '', '', 'https://itunes.apple.com/us/app/letstalknow/id978376140?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 09:24:35', '2016-03-02 13:06:57'),
(7, 'IronmongeryDirect', '', '', 'Access the UK\'s biggest range of Ironmongery in stock for next day delivery! Over 14,500 products at unbeatable prices, all available direct from your mobile, and when you order before 8pm, you\'ll get it next day!', '', '1456907982536.png', NULL, 'http://www.ironmongerydirect.co.uk/', 'https://play.google.com/store/apps/details?id=uk.co.ironmongerydirect.trackingapp', 'https://itunes.apple.com/gb/app/ironmongerydirect/id627824525?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 09:30:33', '2016-03-02 13:11:23'),
(8, 'Did I See U', '', '', 'iPhone App development for creating real time connections with people that are literally around you right now, revolutionise the way people meet!', '', '145690784439.png', NULL, '', 'https://play.google.com/store/apps/details?id=com.didiseeuapp', 'https://itunes.apple.com/us/app/did-i-see-u/id929512119?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 09:37:25', '2016-03-02 13:11:49'),
(9, 'Obelisk Tours', '', '', 'Obelisk Tours take you on a wonderful journey along the River Thames - the spine of London - and towards some of the most famous and iconic landmarks of the Capital; through British cities, towns and villages to discover the people and events that changed the course of history.', '', '1456911454409.png', NULL, '', 'https://play.google.com/store/apps/details?id=com.appinstitute.obelisktours', 'https://itunes.apple.com/gb/app/obelisk-tours-tours-london/id838899924?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 10:37:35', '2016-03-02 13:12:18'),
(10, 'IronmongeryDirect Fire Door', '', '', 'Use the IronmongeryDirect Fire Door App to help you select and buy the fire door hardware that is suitable for your job. Just a few simple questions and you\'re done!', '', '1456911894864.png', NULL, 'http://www.ironmongerydirect.co.uk/', 'https://play.google.com/store/apps/details?id=com.appinstitute.firedoor', 'https://itunes.apple.com/gb/app/ironmongerydirect-fire-door/id1033109798?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 10:44:54', '2016-03-02 13:12:49'),
(11, 'CabbieAppUK (Driver\'s App)', '', '', 'CabbieAppUK LTD designed for Hackney carriage taxi drivers only, Nationwide connecting the customers directly to the drivers by App booking and phone calls, drivers receiving direct phone calls from nearest customers also receiving nearest App booking (PDA) job to drivers mobile phone.\r\n', '', '1456912616414.png', NULL, 'http://www.cabbieappuk.com/', 'https://play.google.com/store/apps/details?id=uk.co.taxicallef.cabbieappuk', 'https://itunes.apple.com/gb/app/cabbieappuk-drivers-app/id884452153?mt=8', 'Justin Ahurst', 'AppInstitute', '', '2016-03-02 10:56:57', '2016-03-02 13:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Mobile App', '2015-11-24 12:04:55', '2015-11-24 12:04:55'),
(2, 'WebApp', '2015-11-24 12:04:55', '2015-11-24 12:04:55'),
(3, 'Custom Website', '2015-11-24 12:04:55', '2015-11-24 12:04:55'),
(4, 'Custom Software', '2015-11-24 12:04:55', '2015-11-24 12:04:55'),
(5, 'Miscellaneous', '2015-11-24 12:04:55', '2015-11-24 12:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `project_types_projects`
--

CREATE TABLE `project_types_projects` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_type_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_types_projects`
--

INSERT INTO `project_types_projects` (`id`, `project_id`, `project_type_id`, `created`, `modified`) VALUES
(1, 1, 1, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(2, 1, 4, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(3, 1, 5, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(4, 3, 1, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(5, 3, 2, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(6, 3, 3, '2016-01-21 16:51:47', '2016-01-21 16:51:47'),
(9, 5, 1, NULL, NULL),
(10, 5, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `details` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `details`, `created`, `modified`) VALUES
(1, 'Logo & Graphic Design', 'We all want to make a good first impression, and hopefully a lasting one too! By understanding your companyâ€™s audience, values and goals, we produce logo designs.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.', '2015-11-24 07:54:18', '2015-11-24 07:55:04'),
(2, 'Web Design & Development', 'With over 5 years in website development and 10 go-to web experts on board, *** has created more than 100 efficient and successful websites. ', '<strong>WEBSITE DEVELOPMENT FROM A TO Z</strong>\r\n\r\nKeeping abreast of the evolving web technologies, we have been continuously shaping our services to meet the growing demand for fast, secure and interactive websites:\r\n\r\n    <strong>Full-cycle website design and development:</strong> creating a website from scratch, including requirements gathering, design, implementation, quality assurance as well as maintenance and support\r\n\r\n    <strong>Redesign:</strong> porting your legacy website, including all the data, to a new, modern solution (it can be another content management system) with a slick and responsive user interface.\r\n\r\n    </strong>Maintenance and support:</strong>\r\n        - Creating new features and fixing bugs\r\n        - Enhancing scalability and performance to welcome the growing number of visitors and data\r\n        - Improving the website structure to better address user demand\r\n        - Increasing compliance with SEO standards for advanced content marketing strategies\r\n        - Performing security audit and updates to protect data and users.\r\n\r\n    <strong>Web app development and integration:</strong> enriching your website with out-of-the-box and custom social networking apps, payment solutions, advanced analytics and other tools to increase user engagement.\r\n\r\n    <strong>Mobile-driven development:</strong> adapting your website for mobile phones and tablets of all platforms and screen sizes as well as using the portal as a back-end for a mobile app.\r\n\r\n\r\n<strong>WEBSITES ACROSS BOUNDARIES</strong>\r\n\r\nThanks to 5 years of multi-domain experience in software development, we bring in technological excellence to create websites that achieve various business goals:\r\n\r\n    - Corporate web portals (enterprise information portals)\r\n    - Websites and patient portals for healthcare organisations\r\n    - Customer portals for telecom providers\r\n    - Websites for banks and financial institutions\r\n    - E-commerce solutions\r\n    - Entertainment, news and media websites\r\n    - Education websites\r\n    - Real estate portals\r\n    - Travel portals\r\n    - Intranet portals and many others\r\n\r\n\r\n<strong>WHY OUR WEBSITES SUCCEED</strong>\r\n\r\n*** website solutions become a strategic asset to drive market strategy and growth thanks to their professional look-and-feel and all-round functionality. What makes them stand out is that they are:\r\n\r\n    - Secure, scalable and optimized for high performance, anticipating business growth and security challenges\r\n\r\n    - Mobile-friendly and offer a responsive web design across all browsers and screens\r\n\r\n    - Easy to manage for non-technical users owing to predefined content formatting styles (headings, paragraphs, multimedia and so on) and a smooth navigation between various kinds of digital assets (texts, images, videos and others)\r\n\r\n    - Interactive, media-rich and personalized to make your visitors stay always engaged \r\n\r\n    - Automatically integrated with business systems (CRM, ERP, e-commerce and so on), social media, data analysis tools and mobile apps\r\n\r\n    - Multilingual and multisite to answer the need of multiple regions, communities and other distinguished groups of visitors while using a single access point (content management system) and various levels of permissions\r\n\r\n    - Compliant with SEO best practices\r\n\r\n\r\n\r\n<strong>TECHNOLOGIES</strong>\r\n\r\n*Front-end: HTML5, CSS3, JavaScript\r\n*Back-end: PHP, .NET, Java, Node.js\r\n*CMSs: Drupal, WordPress, Joomla', '2015-11-24 07:56:57', '2016-03-03 00:37:54'),
(3, 'Mobile Apps (iOS & android)', 'Mobile technology has affected virtually all aspects of human life from governments to public and private enterprises to friends and families. Because of its pervasive presence in our day-to-day lives.', 'Mobile technology has affected virtually all aspects of human life, from governments to public and private enterprises to friends and families. Because of its pervasive presence in our day-to-day lives, <strong>mobile applications</strong> have become a powerful tool for business-to-business and business-to-consumer communications. In fact, organisations that do not incorporate mobile technologies create an unnecessary hazard in a highly competitive business environment.\r\n\r\nOur highly satisfied customers and our award-winning innovations demonstrate a deep understanding of mobile technologies. All of that makes *** the right software development partner for your mobility requirements.\r\n<strong>Our offerings:</strong>\r\n*<strong>Enterprise Mobility </strong>\r\nMobile applications have forever changed the way people work and have raised the bar on productivity. The workforce today has the potential, global access to the demands of their jobs, and an unprecedented opportunity for lightning-fast responses to those demands\r\n*** knows how to exploit productivity opportunities with solutions for mobile access to enterprise applications that result in improved sales and communication, and ultimately a greater return on investment.\r\n\r\n*<strong>Customer engaging mobile applications</strong>\r\nCustomer engagement is much more than just maintaining contact information and sales histories. The word â€œengagementâ€ implies a promise to do something; it requires a social response to consumer behaviour. \r\n*** understands how customer engagement in the mobile era can fuel business growth by offering goods and services at the best prices and providing a personalised, always-available, self-service environment to improve overall customer satisfaction, stickiness, and trust. \r\n***\'s expertise in customer engagement solutions encompasses a wide range of business sectors, including: \r\n- Media\r\n- Mobile banking\r\n- Travel\r\n- Retail\r\n- Automotive\r\n- Energy\r\n*<strong>Application Security </strong>\r\nAny organisation involved in processing, storing, or transmitting highly sensitive or private information, must comply with strict privacy and security guidelines, such as the Payment Industry Data Security Standards (PCI-DSS) for electronic payment processing safety. \r\nBecause of the business impact of non-compliance with those security standards, ***\'s software and application security services are specifically designed to identify detrimental software security problems, often before a single line of code is written.\r\n\r\n<strong>Why ***?</strong>\r\n\r\n- Deep understanding of a wide range of business domains\r\n- End-to-End expertise in new mobile apps development and migration, including iPhone app development and Android app development\r\n- Proven ability to support multiple platforms via native or cross-platform technologies\r\n- Short development cycles\r\n- Continuous investment in learning new platforms and best practices\r\n- Real experience in applying Agile development methods for multi-location project execution\r\n- A skilled workforce of app inventors for high quality iPhone and Android applications\r\n\r\n<strong>Mobile application engineering using all mobile capabilities</strong>\r\n- Location-Based Services\r\n- Multi-touch\r\n- Animation\r\n- Networking\r\n- Media and Video\r\n- Synchronisation\r\n<strong>Cross-platform mobile development for all major mobile platforms</strong>\r\n*iOS â†’ Objective-C, Cocoa, OpenGL, sqlite\r\n*Android â†’ Java, OpenGL, sqlite\r\n*Cross-platform â†’ JavaScript, HTML5, WebKit, PhoneGap, Titanium', '2015-11-24 07:57:30', '2016-03-03 00:29:00'),
(4, 'Custom Software', 'Our software engineering process collects and translates business requirements into imaginative technology solutions that become reality with custom software development. ', 'Our software engineering process collects and translates business requirements into imaginative technology solutions that become reality with custom software development. X software application development services deliver efficient and reliable custom software systems, including core business applications and back-office IT solutions.\r\n\r\nWe provide a full spectrum of custom software engineering services:\r\n\r\n    1. Architecture Consulting and Design\r\n    2. Software development\r\n    3. Performance Engineering\r\n    4. Software Quality Assurance and Testing\r\n\r\nTo ensure integration, versatility, transparency and secure collaboration in all of our custom software development projects, x uses a proprietary Web-based solution that drives efficient software application development by diverse and geographically distributed teams. ', '2015-11-24 07:58:00', '2016-03-03 00:29:50');

-- --------------------------------------------------------

--
-- Table structure for table `slider_general_settings`
--

CREATE TABLE `slider_general_settings` (
  `id` int(11) NOT NULL,
  `delay` int(11) DEFAULT NULL,
  `start_width` int(11) DEFAULT NULL,
  `start_height` int(11) DEFAULT NULL,
  `hide_thumbs` int(3) DEFAULT NULL,
  `default_delay` int(11) DEFAULT NULL,
  `default_start_width` int(11) DEFAULT NULL,
  `default_start_height` int(11) DEFAULT NULL,
  `default_hide_thumbs` int(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_general_settings`
--

INSERT INTO `slider_general_settings` (`id`, `delay`, `start_width`, `start_height`, `hide_thumbs`, `default_delay`, `default_start_width`, `default_start_height`, `default_hide_thumbs`, `created`, `modified`) VALUES
(1, 2500, 1170, 500, 10, 9000, 1170, 500, 10, '2015-11-06 16:29:29', '2016-03-01 22:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `id` int(11) NOT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `order` smallint(2) DEFAULT NULL,
  `transition_type` varchar(50) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `default_order` smallint(2) DEFAULT NULL,
  `default_transition` varchar(50) DEFAULT NULL,
  `default_speed` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`id`, `image_link`, `order`, `transition_type`, `speed`, `is_default`, `default_image`, `default_order`, `default_transition`, `default_speed`, `created`, `modified`) VALUES
(1, '../images/145689500073.jpg', 1, 'fade', 1500, 1, '../images/slidebg1.jpg', 1, 'fade', 1500, '2015-11-17 09:37:08', '2016-03-01 23:03:21'),
(2, '../images/1456898595434.jpg', 2, 'zoomout', 1000, 1, '../images/darkblurbg.jpg', 2, 'zoomout', 1000, '2015-11-05 17:08:19', '2016-03-02 00:03:15'),
(3, '../images/1456853514285.jpg', 3, 'slidedown', 1000, 1, '../images/transparent.png', 3, 'slidedown', 1000, '2015-11-05 17:08:16', '2016-03-01 11:31:54'),
(4, '../images/145685353642.jpg', 4, 'slideleft', 2000, 1, '../images/transparent.png', 4, 'slideleft', 2000, '2015-11-05 17:10:42', '2016-03-01 11:32:17'),
(5, '../images/1457000381885.jpg', 5, 'zoomin', 500, 1, '../images/videobg1.jpg', 5, 'zoomin', 500, '2015-11-05 17:12:13', '2016-03-03 04:19:43'),
(8, '../images/1456853589811.jpg', 6, 'fade', 4561, NULL, NULL, NULL, NULL, NULL, '2015-11-18 07:08:42', '2016-03-01 11:33:09'),
(9, '../images/1456853561603.jpg', 7, 'slidedown', 1000, NULL, NULL, NULL, NULL, NULL, '2015-11-24 07:27:11', '2016-03-01 11:32:41'),
(10, '../images/1456921321300.jpg', NULL, 'slideup', NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-01 11:36:43', '2016-03-02 06:22:02');

-- --------------------------------------------------------

--
-- Table structure for table `slider_layers`
--

CREATE TABLE `slider_layers` (
  `id` int(11) NOT NULL,
  `slider_image_id` int(11) DEFAULT NULL,
  `order` int(3) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `data-x` varchar(255) DEFAULT NULL,
  `data-hoffset` varchar(255) DEFAULT NULL,
  `data-y` varchar(255) DEFAULT NULL,
  `data-voffset` varchar(255) DEFAULT NULL,
  `data-customin` varchar(500) DEFAULT NULL,
  `data-customout` varchar(500) DEFAULT NULL,
  `data-speed` int(11) DEFAULT NULL,
  `data-start` int(11) DEFAULT NULL,
  `data-easing` varchar(255) DEFAULT NULL,
  `data-endspeed` int(11) DEFAULT NULL,
  `data-end` int(11) DEFAULT NULL,
  `data-endeasing` varchar(255) DEFAULT NULL,
  `data-captionhidden` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `text` text,
  `image` varchar(255) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_layers`
--

INSERT INTO `slider_layers` (`id`, `slider_image_id`, `order`, `class`, `data-x`, `data-hoffset`, `data-y`, `data-voffset`, `data-customin`, `data-customout`, `data-speed`, `data-start`, `data-easing`, `data-endspeed`, `data-end`, `data-endeasing`, `data-captionhidden`, `content_type`, `text`, `image`, `image_width`, `image_height`, `created`, `modified`) VALUES
(2, 1, 2, 'tp-caption large_bold_white skewfromrightshort customout', '80', '', '96', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 800, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Faster', NULL, NULL, NULL, '2015-11-18 13:21:14', '2016-03-01 23:10:52'),
(3, 1, 3, 'tp-caption medium_text skewfromleftshort customout', '285', '', '122', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 900, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '&', NULL, NULL, NULL, '2015-11-18 13:22:10', '2016-03-01 23:38:36'),
(4, 1, 4, 'tp-caption medium_text skewfromrightshort customout', '120', '', '175', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 300, 1000, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'More', NULL, NULL, NULL, '2015-11-18 13:22:34', '2016-03-02 00:16:42'),
(5, 1, 5, 'tp-caption large_bold_white skewfromleftshort customout', '175', '', '152', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 300, 1100, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Powerful', NULL, NULL, NULL, '2015-11-18 13:22:59', '2016-03-01 23:13:19'),
(6, 1, 6, 'tp-caption small_text customin customout', '80', '', '240', '', 'x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1300, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Slider Revolution is the highly acclaimed<br/> slide-based displaying solution, thousands of<br/> businesses, theme developers and everyday<br/> people use and love!', NULL, NULL, NULL, '2015-11-18 13:23:37', '2016-03-01 23:40:02'),
(7, 1, 7, 'tp-caption skewfromrightshort customout', '816', '', '207', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1750, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', '', '../images/graph.png', 52, 48, '2015-11-18 13:25:40', '2016-03-01 10:59:32'),
(8, 1, 8, 'tp-caption large_bold_darkblue skewfromleftshort customout', 'right', '-90', '60', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1600, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '1000\'s', NULL, NULL, NULL, '2015-11-18 13:26:06', '2015-11-18 13:26:08'),
(9, 1, 9, 'tp-caption medium_bg_darkblue skewfromleftshort customout', 'right', '-90', '125', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1650, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Of Happy Users', NULL, NULL, NULL, '2015-11-18 13:26:35', '2015-11-18 13:26:38'),
(10, 1, 10, 'tp-caption medium_bold_red skewfromrightshort customout', 'right', '-90', '200', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1800, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Record Breaking', NULL, NULL, NULL, '2015-11-18 13:27:01', '2015-11-18 13:27:04'),
(11, 1, 11, 'tp-caption medium_light_red skewfromrightshort customout', 'right', '-90', 'center', '-10', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1850, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Sales on CodeCanyon', NULL, NULL, NULL, '2015-11-18 13:27:33', '2015-11-18 13:27:35'),
(12, 1, 12, 'tp-caption medium_bg_red skewfromrightshort customout', 'right', '-90', 'bottom', '-200', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1900, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'A Professional Solution', NULL, NULL, NULL, '2015-11-18 13:28:04', '2015-11-18 13:28:06'),
(13, 1, 13, 'tp-caption medium_bold_orange skewfromleftshort customout', 'right', '-90', NULL, '340', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 2000, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '4.7', NULL, NULL, NULL, '2015-11-18 13:29:08', '2015-11-18 13:29:10'),
(14, 1, 14, 'tp-caption customin customout', 'right', '-90', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2050, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', '', '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2016-03-01 11:00:56'),
(15, 1, 15, 'tp-caption customin customout', 'right', '-115', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2100, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(16, 1, 16, 'tp-caption customin customout', 'right', '-140', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2150, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(17, 1, 17, 'tp-caption customin customout', 'right', '-165', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2200, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(18, 1, 18, 'tp-caption customin customout', 'right', '-190', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2250, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(19, 1, 19, 'tp-caption medium_bg_orange skewfromleftshort customout', 'right', '-90', 'bottom', '-50', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 2300, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Customer Rating', NULL, NULL, NULL, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(20, 2, 1, 'tp-caption customin', '474', '', '189', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', '', 500, 800, 'Power3.easeInOut', 300, NULL, '', '', 'img', '', '../images/macbook2.png', NULL, NULL, '2015-11-18 14:37:16', '2016-03-02 05:27:57'),
(21, 2, 2, 'tp-caption customin', '245', NULL, '92', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 500, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/imac1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(22, 2, 3, 'tp-caption customin', '693', NULL, '69', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 1300, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/lupe_macbook.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(23, 2, 4, 'tp-caption customin', '100', NULL, '171', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 1400, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/lupe_imac.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(24, 2, 5, 'tp-caption medium_bg_asbestos skewfromleft customout', '104', '', '154', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1500, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'Obelisk Tours (Android & iOS)', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 23:35:08'),
(25, 2, 6, 'tp-caption medium_bg_red skewfromright customout', '820', '', '274', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1700, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'ULibrary (Android & iOS)', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 23:35:20'),
(26, 2, 7, 'tp-caption medium_bg_orange skewfromright customout', '820', '', '314', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1800, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'Capptain\'s Riches (iOS)', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 21:51:30'),
(27, 2, 8, 'tp-caption medium_bg_darkblue skewfromleft customout', '179', '', '193', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1600, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'Poppleston Allen (iOS)', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-03 01:27:52'),
(28, 2, 9, 'tp-caption large_bold_grey customin customout', '428', '', '34', '', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1100, 'Back.easeOut', 300, NULL, 'Power1.easeIn', '', 'txt', 'Our', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 21:48:25'),
(29, 2, 10, 'tp-caption modern_medium_light customin customout', '550', '', '51', '', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1200, 'Back.easeOut', 300, NULL, 'Power1.easeIn', '', 'txt', 'Pride', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 21:57:47'),
(30, 3, 1, 'tp-caption customin skewtoleft', '100', '', '54', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', '', 600, 1500, 'Back.easeOut', 400, NULL, 'Back.easeIn', '', 'img', '', '../images/1480659528477.png', NULL, NULL, '2015-11-18 14:37:18', '2016-12-02 00:18:52'),
(35, 3, 6, 'tp-caption grassfloor lfb ltb', 'center', '0', 'bottom', '50', NULL, NULL, 600, 500, 'Back.easeOut', 600, NULL, 'Power4.easeIn', 'on', 'txt', NULL, ' ', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(41, 3, 12, 'tp-caption customin ltl', '769', NULL, '177', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 2000, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'img', NULL, '../images/icon_photo.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(52, 3, 16, 'tp-caption mediumlarge_light_darkblue customin ltl', '652', '', '282', '', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', '', 1000, 2050, 'Back.easeInOut', 400, NULL, 'Back.easeIn', '', 'txt', 'Design & Development', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-21 04:04:01'),
(57, 4, 5, 'tp-caption lfr', '696', NULL, '377', NULL, NULL, NULL, 600, 800, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill4.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(58, 4, 6, 'tp-caption grassfloor lfb ltr', 'center', '0', 'bottom', '50', NULL, NULL, 600, 0, 'Power4.easeOut', 600, NULL, 'Power4.easeIn', 'on', 'txt', ' ', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(59, 4, 7, 'tp-caption lfr', '464', NULL, '382', NULL, NULL, NULL, 600, 500, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(60, 4, 8, 'tp-caption lfr', '-59', NULL, '366', NULL, NULL, NULL, 600, 600, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(61, 4, 9, 'tp-caption lfr', '857', NULL, '386', NULL, NULL, NULL, 600, 700, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(64, 4, 12, 'tp-caption customin', '110', '', '118', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', '', 300, 1300, 'Back.easeOut', 300, NULL, '', 'on', 'txt', '', '../images/tree.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:15:10'),
(65, 4, 13, 'tp-caption customin customout', 'center', '0', 'bottom', '-250', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 1800, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/clock2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(66, 4, 14, 'tp-caption customin skewtoright', '817', NULL, '197', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 1000, 2000, 'Back.easeOut', 600, NULL, 'Power4.easeIn', NULL, 'img', NULL, '../images/guy3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(67, 4, 15, 'tp-caption customin skewtoright', '946', NULL, '155', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 1000, 2100, 'Back.easeOut', 600, NULL, 'Power4.easeIn', NULL, 'img', NULL, '../images/guy4.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(68, 4, 16, 'tp-caption customin customout', '126', '', '269', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1800, 'Back.easeOut', 600, 4400, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:15:47'),
(69, 4, 17, 'tp-caption customin customout', '167', '', '303', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1500, 'Back.easeOut', 600, 4100, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:15:57'),
(70, 4, 18, 'tp-caption customin customout', '264', '', '304', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1600, 'Back.easeOut', 600, 4200, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:16:09'),
(71, 4, 19, 'tp-caption customin customout', '296', '', '250', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1700, 'Back.easeOut', 600, 4300, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:16:22'),
(72, 4, 20, 'tp-caption customin customout', '223', '', '263', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1400, 'Back.easeOut', 600, 4000, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:16:34'),
(73, 4, 21, 'tp-caption customin customout', '166', '', '256', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1900, 'Back.easeOut', 600, 4500, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:16:44'),
(74, 4, 22, 'tp-caption customin customout', '118', '', '219', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 2000, 'Back.easeOut', 600, 4600, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:16:56'),
(75, 4, 23, 'tp-caption customin customout', '251', '', '208', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 2100, 'Back.easeOut', 600, 4700, 'Bounce.easeOut', 'on', 'txt', '', '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:17:20'),
(77, 5, 1, 'tp-caption medium_bg_darkblue sfr customout', '129', '', '88', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1600, 'Power4.easeOut', 500, NULL, '', 'on', 'txt', 'Logo & Graphic Design', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 05:58:23'),
(78, 5, 2, 'tp-caption medium_bg_red sfr customout', '129', '', '148', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1700, 'Power4.easeOut', 500, NULL, '', 'on', 'txt', 'Web Design & Development', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:02:49'),
(79, 5, 3, 'tp-caption medium_bg_red sfr customout', '129', '', '208', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1800, 'Power4.easeOut', 500, NULL, '', 'on', 'txt', 'Mobile Apps (iOS & android)', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:03:30'),
(80, 5, 4, 'tp-caption medium_bg_red sfr customout', '129', '', '268', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1900, 'Power4.easeOut', 500, NULL, '', 'on', 'txt', 'Custom Software', NULL, NULL, NULL, '2015-11-18 14:37:18', '2016-03-02 06:03:47'),
(84, 8, 1, 'tp-caption medium_bg_darkblue sfr customout', '329', '', '288', '', '', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1600, 'Power4.easeOut', 500, NULL, '', 'on', 'txt', '', NULL, NULL, NULL, '2015-11-20 06:17:44', '2016-03-21 03:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `slider_layers_default`
--

CREATE TABLE `slider_layers_default` (
  `id` int(11) NOT NULL,
  `slider_image_id` int(11) DEFAULT NULL,
  `order` int(3) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `data-x` varchar(255) DEFAULT NULL,
  `data-hoffset` varchar(255) DEFAULT NULL,
  `data-y` varchar(255) DEFAULT NULL,
  `data-voffset` varchar(255) DEFAULT NULL,
  `data-customin` varchar(500) DEFAULT NULL,
  `data-customout` varchar(500) DEFAULT NULL,
  `data-speed` int(11) DEFAULT NULL,
  `data-start` int(11) DEFAULT NULL,
  `data-easing` varchar(255) DEFAULT NULL,
  `data-endspeed` int(11) DEFAULT NULL,
  `data-end` int(11) DEFAULT NULL,
  `data-endeasing` varchar(255) DEFAULT NULL,
  `data-captionhidden` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `text` text,
  `image` varchar(255) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_layers_default`
--

INSERT INTO `slider_layers_default` (`id`, `slider_image_id`, `order`, `class`, `data-x`, `data-hoffset`, `data-y`, `data-voffset`, `data-customin`, `data-customout`, `data-speed`, `data-start`, `data-easing`, `data-endspeed`, `data-end`, `data-endeasing`, `data-captionhidden`, `content_type`, `text`, `image`, `image_width`, `image_height`, `created`, `modified`) VALUES
(1, 1, 1, 'tp-caption customin customout', 'center', '100', 'bottom', '0', 'x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 700, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', NULL, 'img', '', '../images/woman.png', NULL, NULL, '2015-11-17 09:51:45', '2015-11-17 09:51:47'),
(2, 1, 2, 'tp-caption large_bold_grey skewfromrightshort customout', '80', NULL, '96', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 800, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Faster', NULL, NULL, NULL, '2015-11-18 13:21:14', '2015-11-18 13:21:17'),
(3, 1, 3, 'tp-caption medium_thin_grey skewfromleftshort customout', '285', NULL, '122', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 900, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '&', NULL, NULL, NULL, '2015-11-18 13:22:10', '2015-11-18 13:22:13'),
(4, 1, 4, 'tp-caption medium_thin_grey skewfromrightshort customout', '80', NULL, '175', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 300, 1000, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'More', NULL, NULL, NULL, '2015-11-18 13:22:34', '2015-11-18 13:22:37'),
(5, 1, 5, 'tp-caption large_bold_grey skewfromleftshort customout', '175', NULL, '152', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 300, 1100, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Powerful', NULL, NULL, NULL, '2015-11-18 13:22:59', '2015-11-18 13:23:01'),
(6, 1, 6, 'tp-caption small_thin_grey customin customout', '80', NULL, '240', NULL, 'x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1300, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Slider Revolution is the highly acclaimed<br/> slide-based displaying solution, thousands of<br/> businesses, theme developers and everyday<br/> people use and love!', NULL, NULL, NULL, '2015-11-18 13:23:37', '2015-11-18 13:23:40'),
(7, 1, 7, 'tp-caption skewfromrightshort customout', '816', NULL, '207', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1750, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/graph.png', 52, 48, '2015-11-18 13:25:40', '2015-11-18 13:25:42'),
(8, 1, 8, 'tp-caption large_bold_darkblue skewfromleftshort customout', 'right', '-90', '60', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1600, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '1000\'s', NULL, NULL, NULL, '2015-11-18 13:26:06', '2015-11-18 13:26:08'),
(9, 1, 9, 'tp-caption medium_bg_darkblue skewfromleftshort customout', 'right', '-90', '125', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1650, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Of Happy Users', NULL, NULL, NULL, '2015-11-18 13:26:35', '2015-11-18 13:26:38'),
(10, 1, 10, 'tp-caption medium_bold_red skewfromrightshort customout', 'right', '-90', '200', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1800, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Record Breaking', NULL, NULL, NULL, '2015-11-18 13:27:01', '2015-11-18 13:27:04'),
(11, 1, 11, 'tp-caption medium_light_red skewfromrightshort customout', 'right', '-90', 'center', '-10', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1850, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Sales on CodeCanyon', NULL, NULL, NULL, '2015-11-18 13:27:33', '2015-11-18 13:27:35'),
(12, 1, 12, 'tp-caption medium_bg_red skewfromrightshort customout', 'right', '-90', 'bottom', '-200', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1900, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'A Professional Solution', NULL, NULL, NULL, '2015-11-18 13:28:04', '2015-11-18 13:28:06'),
(13, 1, 13, 'tp-caption medium_bold_orange skewfromleftshort customout', 'right', '-90', NULL, '340', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 2000, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', '4.7<span style=\"font-weight:300;\">/5 Stars</span>', NULL, NULL, NULL, '2015-11-18 13:29:08', '2015-11-18 13:29:10'),
(14, 1, 14, 'tp-caption customin customout', 'right', '-90', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2050, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(15, 1, 15, 'tp-caption customin customout', 'right', '-115', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2100, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(16, 1, 16, 'tp-caption customin customout', 'right', '-140', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2150, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(17, 1, 17, 'tp-caption customin customout', 'right', '-165', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2200, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(18, 1, 18, 'tp-caption customin customout', 'right', '-190', 'bottom', '-103', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 2250, 'Power4.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'img', NULL, '../images/star.png', 20, 20, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(19, 1, 19, 'tp-caption medium_bg_orange skewfromleftshort customout', 'right', '-90', 'bottom', '-50', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 2300, 'Back.easeOut', 500, NULL, 'Power4.easeIn', 'on', 'txt', 'Customer Rating', NULL, NULL, NULL, '2015-11-18 13:29:08', '2015-11-18 13:29:08'),
(20, 2, 1, 'tp-caption customin', '474', NULL, '189', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 800, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/macbook2.png', NULL, NULL, '2015-11-18 14:37:16', '2015-11-18 14:37:18'),
(21, 2, 2, 'tp-caption customin', '245', NULL, '92', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 500, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/imac1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(22, 2, 3, 'tp-caption customin', '693', NULL, '69', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 1300, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/lupe_macbook.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(23, 2, 4, 'tp-caption customin', '100', NULL, '171', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 500, 1400, 'Power3.easeInOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/lupe_imac.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(24, 2, 5, 'tp-caption medium_bg_asbestos skewfromleft customout', '104', NULL, '154', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1500, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'Caption Selection', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(25, 2, 6, 'tp-caption medium_bg_red skewfromright customout', '820', NULL, '274', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1700, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'Custom Animation Editor', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(26, 2, 7, 'tp-caption medium_bg_orange skewfromright customout', '820', NULL, '314', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1800, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'With Live Preview', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(27, 2, 8, 'tp-caption medium_bg_darkblue skewfromleft customout', '168', NULL, '193', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 800, 1600, 'Power4.easeOut', 300, NULL, 'Power1.easeIn', 'on', 'txt', 'With Style Preview', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(28, 2, 9, 'tp-caption large_bold_white customin customout', '428', NULL, '34', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1100, 'Back.easeOut', 300, NULL, 'Power1.easeIn', NULL, 'txt', 'Big', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(29, 2, 10, 'tp-caption medium_light_white customin customout', '536', NULL, '51', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1200, 'Back.easeOut', 300, NULL, 'Power1.easeIn', NULL, 'txt', 'Improvements', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(30, 3, 1, 'tp-caption customin skewtoleft', '877', NULL, '54', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 600, 1500, 'Back.easeOut', 400, NULL, 'Back.easeIn', NULL, 'img', NULL, '../images/cloud2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(31, 3, 2, 'tp-caption customin skewtoleft', '84', NULL, '80', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 600, 1300, 'Back.easeOut', 400, NULL, 'Back.easeIn', NULL, 'img', NULL, '../images/cloud3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(32, 3, 3, 'tp-caption customin skewtoleft', '473', NULL, '123', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 600, 1700, 'Back.easeOut', 400, NULL, 'Back.easeIn', NULL, 'img', NULL, '../images/cloud1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(33, 3, 4, 'tp-caption sfb ltl', '639', NULL, '346', NULL, NULL, NULL, 600, 1200, 'Back.easeOut', 400, NULL, 'Back.easeIn', 'on', 'img', NULL, '../images/hill3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(34, 3, 5, 'tp-caption sfb ltl', '228', NULL, '360', NULL, NULL, NULL, 600, 1100, 'Back.easeOut', 400, NULL, 'Back.easeIn', 'on', 'img', NULL, '../images/hill4.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(35, 3, 6, 'tp-caption grassfloor lfb ltb', 'center', '0', 'bottom', '50', NULL, NULL, 600, 500, 'Back.easeOut', 600, NULL, 'Power4.easeIn', 'on', 'txt', NULL, ' ', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(36, 3, 7, 'tp-caption sfb ltl', '142', NULL, '375', NULL, NULL, NULL, 600, 800, 'Back.easeOut', 400, NULL, 'Back.easeIn', 'on', 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(37, 3, 8, 'tp-caption sfb ltl', '496', NULL, '367', NULL, NULL, NULL, 600, 900, 'Back.easeOut', 400, NULL, 'Back.easeIn', 'on', 'img', NULL, '../images/hill1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(38, 3, 9, 'tp-caption sfb ltl', '918', NULL, '379', NULL, NULL, NULL, 600, 1000, 'Back.easeOut', 400, NULL, 'Back.easeIn', 'on', 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(39, 3, 10, 'tp-caption lfb skewtoleft', '122', NULL, '133', NULL, NULL, NULL, 2000, 1300, 'Power4.easeOut', 400, NULL, 'Power1.easeIn', NULL, 'img', NULL, '../images/guy1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(40, 3, 11, 'tp-caption lfb skewtoleft', '318', NULL, '91', NULL, NULL, NULL, 2000, 1400, 'Power4.easeOut', 400, NULL, 'Power1.easeIn', NULL, 'img', NULL, '../images/guy2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(41, 3, 12, 'tp-caption customin ltl', '769', NULL, '177', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 2000, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'img', NULL, '../images/icon_photo.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(42, 3, 13, 'tp-caption large_bold_white customin ltl', '596', NULL, '101', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 1850, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'txt', 'Design', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(43, 3, 14, 'tp-caption medium_light_white customin ltl', '813', NULL, '124', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 1900, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'txt', '&', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(44, 3, 15, 'tp-caption large_bold_white customin ltl', '845', NULL, '102', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 1950, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'txt', 'Create', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(52, 3, 16, 'tp-caption mediumlarge_light_white customin ltl', '652', NULL, '282', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', NULL, 1000, 2050, 'Back.easeInOut', 400, NULL, 'Back.easeIn', NULL, 'txt', 'With Total Layer Control', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(53, 4, 1, 'tp-caption lfr', '889', NULL, '118', NULL, NULL, NULL, 600, 1000, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/cloud2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(54, 4, 2, 'tp-caption lfr', '339', NULL, '67', NULL, NULL, NULL, 600, 1100, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/cloud3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(55, 4, 3, 'tp-caption lfr', '12', NULL, '181', NULL, NULL, NULL, 600, 1200, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/cloud1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(56, 4, 4, 'tp-caption lfr', '120', NULL, '352', NULL, NULL, NULL, 600, 900, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(57, 4, 5, 'tp-caption lfr', '696', NULL, '377', NULL, NULL, NULL, 600, 800, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill4.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(58, 4, 6, 'tp-caption grassfloor lfb ltr', 'center', '0', 'bottom', '50', NULL, NULL, 600, 0, 'Power4.easeOut', 600, NULL, 'Power4.easeIn', 'on', 'txt', ' ', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(59, 4, 7, 'tp-caption lfr', '464', NULL, '382', NULL, NULL, NULL, 600, 500, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(60, 4, 8, 'tp-caption lfr', '-59', NULL, '366', NULL, NULL, NULL, 600, 600, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill1.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(61, 4, 9, 'tp-caption lfr', '857', NULL, '386', NULL, NULL, NULL, 600, 700, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/hill2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(62, 4, 10, 'tp-caption large_bold_white customin customout', 'center', '0', '80', NULL, 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 1700, 'Back.easeInOut', 300, NULL, NULL, NULL, 'txt', 'Timelined Captions', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(63, 4, 11, 'tp-caption mediumlarge_light_white_center customin customout', 'center', '0', 'bottom', '-120', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 1900, 'Back.easeInOut', 300, NULL, NULL, NULL, 'txt', 'Create Custom Animations<br/>\r\n    with our Brand-New<br/>\r\n    Transition Builder', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(64, 4, 12, 'tp-caption customin', '110', NULL, '118', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 300, 1300, 'Back.easeOut', 300, NULL, NULL, 'on', 'img', NULL, '../images/tree.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(65, 4, 13, 'tp-caption customin customout', 'center', '0', 'bottom', '-250', 'x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 1800, 'Back.easeOut', 300, NULL, NULL, NULL, 'img', NULL, '../images/clock2.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(66, 4, 14, 'tp-caption customin skewtoright', '817', NULL, '197', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 1000, 2000, 'Back.easeOut', 600, NULL, 'Power4.easeIn', NULL, 'img', NULL, '../images/guy3.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(67, 4, 15, 'tp-caption customin skewtoright', '946', NULL, '155', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', NULL, 1000, 2100, 'Back.easeOut', 600, NULL, 'Power4.easeIn', NULL, 'img', NULL, '../images/guy4.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(68, 4, 16, 'tp-caption customin customout', '126', NULL, '269', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1800, 'Back.easeOut', 600, 4400, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(69, 4, 17, 'tp-caption customin customout', '167', NULL, '303', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1500, 'Back.easeOut', 600, 4100, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(70, 4, 18, 'tp-caption customin customout', '264', NULL, '304', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1600, 'Back.easeOut', 600, 4200, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(71, 4, 19, 'tp-caption customin customout', '296', NULL, '250', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1700, 'Back.easeOut', 600, 4300, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(72, 4, 20, 'tp-caption customin customout', '223', NULL, '263', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1400, 'Back.easeOut', 600, 4000, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(73, 4, 21, 'tp-caption customin customout', '166', NULL, '256', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1900, 'Back.easeOut', 600, 4500, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(74, 4, 22, 'tp-caption customin customout', '118', NULL, '219', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 2000, 'Back.easeOut', 600, 4600, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(75, 4, 23, 'tp-caption customin customout', '251', NULL, '208', NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 2100, 'Back.easeOut', 600, 4700, 'Bounce.easeOut', 'on', 'img', NULL, '../images/apple.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(77, 5, 1, 'tp-caption medium_bg_darkblue sfr customout', '129', NULL, '88', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1600, 'Power4.easeOut', 500, NULL, NULL, 'on', 'txt', 'Extensive Video Support', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(78, 5, 2, 'tp-caption medium_bg_red sfr customout', '293', NULL, '130', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1700, 'Power4.easeOut', 500, NULL, NULL, 'on', 'txt', 'Youtube', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(79, 5, 3, 'tp-caption medium_bg_red sfr customout', '314', NULL, '173', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1800, 'Power4.easeOut', 500, NULL, NULL, 'on', 'txt', 'Vimeo', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(80, 5, 4, 'tp-caption medium_bg_red sfr customout', '184', NULL, '216', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 1900, 'Power4.easeOut', 500, NULL, NULL, 'on', 'txt', 'Self-Hosted HTML 5', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(81, 5, 5, 'tp-caption medium_bg_orange sfr customout', '90', NULL, '357', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 500, 2200, 'Power4.easeOut', 500, NULL, NULL, 'on', 'txt', 'Next Slide: Full Screen Video', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(82, 5, 6, 'tp-caption sft customout', '399', NULL, '385', NULL, NULL, 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 1000, 1500, 'Back.easeOut', 500, NULL, NULL, 'on', 'img', NULL, '../images/videoshadow.png', NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18'),
(83, 5, 7, 'tp-caption customin customout', 'center', '134', 'center', '-6', 'x:0;y:0;z:0;rotationX:0;rotationY:100;rotationZ:0;scaleX:10;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 'x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;', 600, 1000, 'Power4.easeOut', 500, NULL, 'Power4.easeOut', NULL, 'vdo', '<iframe src=\'http://player.vimeo.com/video/76512663?title=0&byline=0&portrait=0;api=1\' width=\'640\' height=\'360\' style=\'width:640px;height:360px;\'></iframe>', NULL, NULL, NULL, '2015-11-18 14:37:18', '2015-11-18 14:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `order` tinyint(2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `name`, `icon`, `link`, `active`, `order`, `created`, `modified`) VALUES
(1, 'facebook', '144620215955.png', 'https://www.facebook.com/xorsolution', 1, 1, '2015-10-30 11:08:22', '2018-01-20 09:54:12'),
(3, 'Twitter', '144620219414.png', 'https://twitter.com/', 1, 2, '2015-10-30 11:49:54', '2015-10-30 11:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menus`
--

CREATE TABLE `sub_menus` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `order` smallint(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_menus`
--

INSERT INTO `sub_menus` (`id`, `menu_id`, `name`, `link`, `order`, `active`, `created`, `modified`) VALUES
(1, 1, 'subHome', '/subHome', 1, 1, '2015-11-20 09:24:23', '2015-11-20 09:24:23'),
(2, 1, 'subHome2', '/subHome2', 2, 1, '2015-11-20 09:30:48', '2015-11-20 09:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `work_role` varchar(255) DEFAULT NULL,
  `description` text,
  `contact_1` varchar(255) DEFAULT NULL,
  `contact_2` varchar(255) DEFAULT NULL,
  `mail_1` varchar(255) DEFAULT NULL,
  `mail_2` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `full_name`, `display_name`, `image_link`, `designation`, `work_role`, `description`, `contact_1`, `contact_2`, `mail_1`, `mail_2`, `active`, `created`, `modified`) VALUES
(1, 'Razbul Ahmed', 'Raz Ahmed', '1491506037358.jpg', 'Founder & CEO', '', '', '+8801724160299', '', 'raz@xorbd.com', 'raz.abcoder@gmail.com', 1, '2016-03-02 16:27:28', '2018-01-21 01:33:34'),
(9, 'Shuvro Pal ', 'Shuvro Pal ', '1456920938434.jpg', 'Sr. Software Engr', 'Anroid Developer', '', '', '', 'shuvro@xorbd.com', '', 1, '2016-03-02 12:15:08', '2016-03-02 13:15:39'),
(10, 'Al-mamun Bin Mozid', 'Al-mamun', '1516118906842.jpg', 'Sr. Software Engineer', 'iOS Developer', '', '', '', 'mamun@xorbd.com', '', 1, '2016-03-02 12:16:28', '2018-01-16 10:08:26'),
(12, 'Dipankar Das', 'Dipankar Das', '1456920962575.jpg', 'iOS Developer', 'iOS App Developmeny', '', '', '', 'dipankar@xorbd.com', '', 1, '2016-03-02 12:18:47', '2016-12-02 00:09:28'),
(19, 'Rahul Dev', 'Rahul Dev', '1456921099355.jpg', 'Sr. Software Engineer', 'Web Developer', '', '', '', 'rahul@xorbd.com', '', 1, '2016-03-02 12:32:50', '2016-03-02 13:18:20'),
(20, 'Alamin Hossain', 'Alamin', '1507178959883.jpg', 'Web Designer', 'Design and responsive website', 'Design and responsive website', '', '', '', '', 1, '2017-10-04 23:49:19', '2017-10-04 23:49:19'),
(21, 'Chayan Mistry', 'Chayan Mistry', '1516117029902.jpg', 'Android Apps Developer', 'Android Apps Developer', '', '', '', '', '', 1, '2018-01-16 09:37:10', '2018-01-16 09:41:25'),
(22, 'S.m. Waliuzzaman', 'S.m. Waliuzzaman', '1516117264931.jpg', 'Software Engineer', 'Development & Marketing', '', '', '', '', '', 1, '2018-01-16 09:41:04', '2018-01-16 10:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `comment` text,
  `name` varchar(255) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `comment`, `name`, `organization`, `image_link`, `active`, `created`, `modified`) VALUES
(1, 'My customers love being able to make bookings quickly and getting reminders about their appointments through the App.', 'Justin Ashurst', '', '1448361965380.gif', 1, '2015-11-24 11:33:28', '2016-03-02 11:52:56'),
(2, 'The problem isn\'t getting the customers in anymore, it\'s getting them out again!', 'Enrico Talin', '', '1456916337270.jpg', 1, '2015-11-24 11:43:23', '2016-03-02 11:58:57'),
(3, 'I wanted to create a resource that could help inspire and educate my clients. It\'s so easy to update the app with new recipes and by sending Push Notifications, it\'s even easier to tell everyone about it. The feedback I\'ve had has just been amazing, my clients love being able to access recipes right from their smartphone!', 'Eliza Pickering, Nutritionist', '', '1456915852557.png', 1, '2016-03-02 11:50:52', '2016-03-02 11:50:52'),
(4, 'The app attracts a lot of attention from our customers!', 'Joanne Graham', '', '1456915918143.png', 1, '2016-03-02 11:51:58', '2016-03-02 11:51:58'),
(5, 'It\'s so easy and quick to browse through my store and search for specific bikes, my customers love it.', 'Greg Rodas', '', '1456916084397.png', 1, '2016-03-02 11:54:44', '2016-03-02 11:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `simple_pwd` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `simple_pwd`, `role`, `created`, `modified`) VALUES
(1, 'admin', '55e025d74eee8ad8e4c69a5988716d7961ad84fb', 'adm!@#$%^in', 'admin', '2015-10-29 12:20:28', '2016-03-24 05:44:26'),
(2, 'shafiq', '6b760442f1d41b2b62dd88ec0ea95ca2b5e9c0c7', 's', 'member', '2015-10-29 12:14:42', '2015-10-29 12:14:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_informations`
--
ALTER TABLE `contact_informations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_orders`
--
ALTER TABLE `custom_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_types_projects`
--
ALTER TABLE `project_types_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_general_settings`
--
ALTER TABLE `slider_general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_layers`
--
ALTER TABLE `slider_layers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_layers_default`
--
ALTER TABLE `slider_layers_default`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_menus`
--
ALTER TABLE `sub_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_informations`
--
ALTER TABLE `contact_informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_orders`
--
ALTER TABLE `custom_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `project_types_projects`
--
ALTER TABLE `project_types_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slider_general_settings`
--
ALTER TABLE `slider_general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `slider_layers`
--
ALTER TABLE `slider_layers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `slider_layers_default`
--
ALTER TABLE `slider_layers_default`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `social_links`
--
ALTER TABLE `social_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_menus`
--
ALTER TABLE `sub_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
